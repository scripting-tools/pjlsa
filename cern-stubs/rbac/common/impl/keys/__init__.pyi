
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.rbac.common
import java.security
import java.util
import typing



class KeyStoreLoader:
    """
    public final class KeyStoreLoader extends java.lang.Object
    """
    @staticmethod
    def loadKeyStore(string: str, string2: str) -> java.security.KeyStore: ...
    @staticmethod
    def loadTokenKeyStore(environment: cern.rbac.common.RbacConfiguration.Environment) -> java.security.KeyStore: ...

class RbaKeyStore:
    """
    public final class RbaKeyStore extends java.lang.Object
    
        This class provides access to various keystores and passwords stored in local files.
    """
    KEYSTORE_TYPE: typing.ClassVar[str] = ...
    """
    public static final java.lang.String KEYSTORE_TYPE
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    @staticmethod
    def getPublicKeys(environment: cern.rbac.common.RbacConfiguration.Environment) -> java.util.Collection[java.security.PublicKey]:
        """
            Loads public keys from the public KeyStore and returns them to the caller. Retrieved keys can be used to verify RBAC
            token signature.
        
            Parameters:
                environment (:class:`~cern.rbac.common.RbacConfiguration.Environment`): The runtime environment
        
            Returns:
                Array of available public keys from the public KeyStore
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.rbac.common.impl.keys")``.

    KeyStoreLoader: typing.Type[KeyStoreLoader]
    RbaKeyStore: typing.Type[RbaKeyStore]
