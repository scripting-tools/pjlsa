
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.util.concurrent
import typing



class ExecutionService:
    """
    public interface ExecutionService
    
        TODO: to be removed, unnecessary interface and implementation This service is wrapper over java :code:`ExecutorService`
        and provides API for submitting new tasks which should be executed by an another thread. However it is up to the service
        to decide what policy to use for managing the worker threads (single, pool, cache, etc.).
    """
    _submit__T = typing.TypeVar('_submit__T')  # <T>
    def submit(self, callable: typing.Union[java.util.concurrent.Callable[_submit__T], typing.Callable[[], _submit__T]]) -> java.util.concurrent.Future[_submit__T]: ...

class ExecutionServiceImpl(ExecutionService):
    """
    public class ExecutionServiceImpl extends java.lang.Object implements :class:`~cern.rbac.common.impl.exec.ExecutionService`
    
        TODO: to be removed, unnecessary interface and implementation Default implementation of the
        :class:`~cern.rbac.common.impl.exec.ExecutionService` interface which executes submitted tasks in a cached thread pool,
        which may reuse same thread for subsequent executions.
    """
    def __init__(self): ...
    _submit__T = typing.TypeVar('_submit__T')  # <T>
    def submit(self, callable: typing.Union[java.util.concurrent.Callable[_submit__T], typing.Callable[[], _submit__T]]) -> java.util.concurrent.Future[_submit__T]:
        """
        
            Specified by:
                :meth:`~cern.rbac.common.impl.exec.ExecutionService.submit`Â in
                interfaceÂ :class:`~cern.rbac.common.impl.exec.ExecutionService`
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.rbac.common.impl.exec_")``.

    ExecutionService: typing.Type[ExecutionService]
    ExecutionServiceImpl: typing.Type[ExecutionServiceImpl]
