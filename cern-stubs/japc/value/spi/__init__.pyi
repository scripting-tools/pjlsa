
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.japc.value.spi.util
import cern.japc.value.spi.value
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.japc.value.spi")``.

    util: cern.japc.value.spi.util.__module_protocol__
    value: cern.japc.value.spi.value.__module_protocol__
