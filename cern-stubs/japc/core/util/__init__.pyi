
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.japc.core
import cern.japc.value
import java.lang
import java.util
import typing



class JapcExceptions:
    """
    public class JapcExceptions extends java.lang.Object
    
        Utilities methods related to Exception validation.
    """
    @staticmethod
    def isNoDataAvailableException(throwable: java.lang.Throwable) -> bool:
        """
        
            Parameters:
                throwable (java.lang.Throwable): a Throwable to check
        
            Returns:
                :code:`true` if the given Throwable is an instance of :class:`~cern.japc.core.NoDataAvailableException` and
                :code:`false` otherwise
        
        
        """
        ...
    @staticmethod
    def isNoValueException(throwable: java.lang.Throwable) -> bool:
        """
        
            Parameters:
                throwable (java.lang.Throwable): a Throwable to check
        
            Returns:
                :code:`true` if the given Throwable is an instance of :class:`~cern.japc.core.NoValueException` and :code:`false`
                otherwise
        
        
        """
        ...
    @staticmethod
    def isWaitingForDataException(throwable: java.lang.Throwable) -> bool:
        """
        
            Parameters:
                throwable (java.lang.Throwable): a Throwable to check
        
            Returns:
                :code:`true` if the given Throwable is an instance of :class:`~cern.japc.core.WaitingForDataException` and :code:`false`
                otherwise
        
        
        """
        ...

class JapcUtils:
    """
    public class JapcUtils extends java.lang.Object
    """
    def __init__(self): ...
    @typing.overload
    @staticmethod
    def getValuesAsync(set: java.util.Set[cern.japc.core.ImmutableParameter], selector: cern.japc.core.Selector) -> java.util.Map[str, cern.japc.core.FailSafeParameterValue]: ...
    @typing.overload
    @staticmethod
    def getValuesAsync(set: java.util.Set[cern.japc.core.ImmutableParameter], selector: cern.japc.core.Selector, long: int) -> java.util.Map[str, cern.japc.core.FailSafeParameterValue]: ...
    @typing.overload
    @staticmethod
    def setValuesAsync(map: typing.Union[java.util.Map[cern.japc.core.Parameter, cern.japc.value.ParameterValue], typing.Mapping[cern.japc.core.Parameter, cern.japc.value.ParameterValue]], selector: cern.japc.core.Selector) -> java.util.Map[str, cern.japc.core.FailSafeParameterValue]: ...
    @typing.overload
    @staticmethod
    def setValuesAsync(map: typing.Union[java.util.Map[cern.japc.core.Parameter, cern.japc.value.ParameterValue], typing.Mapping[cern.japc.core.Parameter, cern.japc.value.ParameterValue]], selector: cern.japc.core.Selector, long: int) -> java.util.Map[str, cern.japc.core.FailSafeParameterValue]: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.japc.core.util")``.

    JapcExceptions: typing.Type[JapcExceptions]
    JapcUtils: typing.Type[JapcUtils]
