
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.diag.config
import cern.accsoft.commons.diag.matcher
import cern.accsoft.commons.util
import java.io
import java.lang
import typing



class ApplicationThrowableMatcher(cern.accsoft.commons.diag.matcher.StringThrowableMatcher):
    """
    public class ApplicationThrowableMatcher extends :class:`~cern.accsoft.commons.diag.matcher.StringThrowableMatcher`
    """
    APPLICATION_PROBLEM_DOMAIN: typing.ClassVar[str] = ...
    """
    public static final java.lang.String APPLICATION_PROBLEM_DOMAIN
    
        InCA problem domain
    
        Also see:
            :meth:`~constant`
    
    
    """
    APPLICATION_MATCHER_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String APPLICATION_MATCHER_NAME
    
        InCA matcher name
    
        Also see:
            :meth:`~constant`
    
    
    """
    def __init__(self): ...
    def buildThrowableDescriptor(self, throwable: java.lang.Throwable, throwable2: java.lang.Throwable) -> 'ThrowableDescriptor':
        """
        
            Overrides:
                :meth:`~cern.accsoft.commons.diag.matcher.AbstractThrowableMatcher.buildThrowableDescriptor`Â in
                classÂ :class:`~cern.accsoft.commons.diag.matcher.AbstractThrowableMatcher`
        
        
        """
        ...

_HierarchyImpl__T = typing.TypeVar('_HierarchyImpl__T', bound=cern.accsoft.commons.util.Named)  # <T>
class HierarchyImpl(typing.Generic[_HierarchyImpl__T]):
    """
    public abstract class HierarchyImpl<T extends cern.accsoft.commons.util.Named> extends java.lang.Object
    
        General implementation of a container for objects respecting their order.
    """
    def __init__(self): ...
    def addChildToEnd(self, t: _HierarchyImpl__T) -> None: ...
    def addChildToTop(self, t: _HierarchyImpl__T) -> None: ...

class ThrowableConstants:
    """
    public interface ThrowableConstants
    """
    INDENTATION: typing.ClassVar[str] = ...
    """
    static final java.lang.String INDENTATION
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BR: typing.ClassVar[str] = ...
    """
    static final java.lang.String BR
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    END: typing.ClassVar[str] = ...
    """
    static final java.lang.String END
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    START_NAME: typing.ClassVar[str] = ...
    """
    static final java.lang.String START_NAME
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    START_CHILDREN: typing.ClassVar[str] = ...
    """
    static final java.lang.String START_CHILDREN
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    RESOLVER: typing.ClassVar[str] = ...
    """
    static final java.lang.String RESOLVER
    
    
        Also see:
            :meth:`~constant`
    
    
    """

class ThrowableDescriptor(java.io.Serializable):
    """
    public class ThrowableDescriptor extends java.lang.Object implements java.io.Serializable
    
        Descriptor for a :code:`Throwable`.
    
        Also see:
            :meth:`~serialized`
    """
    def getContactUrl(self) -> str: ...
    def getFullThrowableStackTrace(self) -> str: ...
    def getHintString(self) -> str: ...
    def getHintUrl(self) -> str: ...
    def getMatchedThrowableStackTrace(self) -> str: ...
    def getMatcherName(self) -> str: ...
    def getMessage(self) -> str: ...
    def getProblemDomain(self) -> str: ...
    def toString(self) -> str:
        """
        
            Overrides:
                :code:`toString` in class :code:`java.lang.Object`
        
        
        """
        ...
    class ThrowableDescriptorBuilder:
        @typing.overload
        def __init__(self): ...
        @typing.overload
        def __init__(self, throwableDescriptor: 'ThrowableDescriptor'): ...
        def build(self) -> 'ThrowableDescriptor': ...
        def contactUrl(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...
        def fullThrowableStackTrace(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...
        def hintString(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...
        def hintUrl(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...
        def matchedThrowableStackTrace(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...
        def matcherName(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...
        def message(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...
        def problemDomain(self, string: str) -> 'ThrowableDescriptor.ThrowableDescriptorBuilder': ...

class ThrowableMessageComposer:
    """
    public interface ThrowableMessageComposer
    
        Composes a message describing a :code:`Throwable`.
    """
    def composeMessage(self, throwable: java.lang.Throwable) -> str:
        """
        
            Parameters:
                throwable (java.lang.Throwable): 
            Returns:
                message describing a :code:`Throwable`
        
        
        """
        ...

class ThrowableResolver:
    """
    public interface ThrowableResolver
    
        Resolver for throwables.
    """
    def resolve(self, throwable: java.lang.Throwable) -> ThrowableDescriptor:
        """
        
            Parameters:
                throwable (java.lang.Throwable): 
            Returns:
                the :code:`Throwable`'s description
        
        
        """
        ...

class DefaultThrowableMessageComposer(ThrowableMessageComposer):
    """
    public class DefaultThrowableMessageComposer extends java.lang.Object implements :class:`~cern.accsoft.commons.diag.ThrowableMessageComposer`
    
        Basic implementation of :class:`~cern.accsoft.commons.diag.ThrowableMessageComposer`.
    """
    def __init__(self): ...
    def composeMessage(self, throwable: java.lang.Throwable) -> str:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.diag.ThrowableMessageComposer.composeMessage`Â in
                interfaceÂ :class:`~cern.accsoft.commons.diag.ThrowableMessageComposer`
        
            Returns:
                message describing a :code:`Throwable`
        
        
        """
        ...

class EmptyThrowableResolver(cern.accsoft.commons.diag.matcher.ThrowableMatcherHierarchyImpl, ThrowableResolver):
    """
    public abstract class EmptyThrowableResolver extends :class:`~cern.accsoft.commons.diag.matcher.ThrowableMatcherHierarchyImpl` implements :class:`~cern.accsoft.commons.diag.ThrowableResolver`
    
        :class:`~cern.accsoft.commons.diag.ThrowableResolver` which does not contain any matcher by default.
    
        Matchers can be added manually via :code:`#addChildToTop(ThrowableMatcher)` and
        :code:`#addChildToEnd(ThrowableMatcher)`.
    """
    def __init__(self): ...
    def resolve(self, throwable: java.lang.Throwable) -> ThrowableDescriptor:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.diag.ThrowableResolver.resolve`Â in
                interfaceÂ :class:`~cern.accsoft.commons.diag.ThrowableResolver`
        
            Returns:
                the :code:`Throwable`'s description
        
        
        """
        ...
    def toString(self) -> str:
        """
        
            Overrides:
                :code:`toString` in class :code:`java.lang.Object`
        
        
        """
        ...

class DefaultThrowableResolver(EmptyThrowableResolver):
    """
    public abstract class DefaultThrowableResolver extends :class:`~cern.accsoft.commons.diag.EmptyThrowableResolver`
    
        :class:`~cern.accsoft.commons.diag.ThrowableResolver` with some default matchers recognizing problems related to RBAC,
        CMW, TGM, CCDB and JAPC.
    """
    ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.diag")``.

    ApplicationThrowableMatcher: typing.Type[ApplicationThrowableMatcher]
    DefaultThrowableMessageComposer: typing.Type[DefaultThrowableMessageComposer]
    DefaultThrowableResolver: typing.Type[DefaultThrowableResolver]
    EmptyThrowableResolver: typing.Type[EmptyThrowableResolver]
    HierarchyImpl: typing.Type[HierarchyImpl]
    ThrowableConstants: typing.Type[ThrowableConstants]
    ThrowableDescriptor: typing.Type[ThrowableDescriptor]
    ThrowableMessageComposer: typing.Type[ThrowableMessageComposer]
    ThrowableResolver: typing.Type[ThrowableResolver]
    config: cern.accsoft.commons.diag.config.__module_protocol__
    matcher: cern.accsoft.commons.diag.matcher.__module_protocol__
