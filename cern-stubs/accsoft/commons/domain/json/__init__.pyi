
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.domain.json.defaultobjects
import cern.accsoft.commons.domain.json.modules
import cern.accsoft.commons.util
import com.fasterxml.jackson.annotation
import com.fasterxml.jackson.core
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.deser.std
import com.fasterxml.jackson.databind.jsontype
import com.fasterxml.jackson.databind.ser.std
import java.util
import java.util.function
import typing



_EnumEmulatorDeserializer__T = typing.TypeVar('_EnumEmulatorDeserializer__T', bound=cern.accsoft.commons.util.Named)  # <T>
class EnumEmulatorDeserializer(com.fasterxml.jackson.databind.deser.std.StdDeserializer[_EnumEmulatorDeserializer__T], typing.Generic[_EnumEmulatorDeserializer__T]):
    """
    public class EnumEmulatorDeserializer<T extends cern.accsoft.commons.util.Named> extends com.fasterxml.jackson.databind.deser.std.StdDeserializer<T>
    
    
        Also see:
            :meth:`~serialized`
    """
    @typing.overload
    def __init__(self, class_: typing.Type[_EnumEmulatorDeserializer__T]): ...
    @typing.overload
    def __init__(self, class_: typing.Type[_EnumEmulatorDeserializer__T], collection: typing.Union[java.util.Collection[typing.Type[_EnumEmulatorDeserializer__T]], typing.Sequence[typing.Type[_EnumEmulatorDeserializer__T]]], function: typing.Union[java.util.function.Function[str, _EnumEmulatorDeserializer__T], typing.Callable[[str], _EnumEmulatorDeserializer__T]]): ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext) -> _EnumEmulatorDeserializer__T: ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, t: _EnumEmulatorDeserializer__T) -> _EnumEmulatorDeserializer__T: ...
    @typing.overload
    def deserializeWithType(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, typeDeserializer: com.fasterxml.jackson.databind.jsontype.TypeDeserializer) -> _EnumEmulatorDeserializer__T: ...
    @typing.overload
    def deserializeWithType(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, typeDeserializer: com.fasterxml.jackson.databind.jsontype.TypeDeserializer, t2: _EnumEmulatorDeserializer__T) -> typing.Any: ...

_EnumEmulatorSerializer__T = typing.TypeVar('_EnumEmulatorSerializer__T', bound=cern.accsoft.commons.util.Named)  # <T>
class EnumEmulatorSerializer(com.fasterxml.jackson.databind.ser.std.StdSerializer[_EnumEmulatorSerializer__T], typing.Generic[_EnumEmulatorSerializer__T]):
    """
    public class EnumEmulatorSerializer<T extends cern.accsoft.commons.util.Named> extends com.fasterxml.jackson.databind.ser.std.StdSerializer<T>
    
        A generic serializer for the CERN enum like objects. It uses as an ID generator the
        :class:`~cern.accsoft.commons.domain.json.NameTypeIdGenerator` since it avoids creating the whole Json tree. It always
        writes the object ID
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, class_: typing.Type[_EnumEmulatorSerializer__T]): ...
    def serialize(self, t: _EnumEmulatorSerializer__T, jsonGenerator: com.fasterxml.jackson.core.JsonGenerator, serializerProvider: com.fasterxml.jackson.databind.SerializerProvider) -> None: ...

class NameTypeIdGenerator(com.fasterxml.jackson.annotation.ObjectIdGenerator[str]):
    """
    public class NameTypeIdGenerator extends com.fasterxml.jackson.annotation.ObjectIdGenerator<java.lang.String>
    
        ID generator that works with :code:`Named` classes since it is calling the :code:`Named.getName()` method. The ID would
        be of the type obj_name@simple_class_name. Names of objects of the same class are assumed to be unique to ensure
        uniqueness of the generated IDs
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, class_: typing.Type[cern.accsoft.commons.util.Named]): ...
    def canUseFor(self, objectIdGenerator: com.fasterxml.jackson.annotation.ObjectIdGenerator[typing.Any]) -> bool:
        """
        
            Specified by:
                :code:`canUseFor` in class :code:`com.fasterxml.jackson.annotation.ObjectIdGenerator<java.lang.String>`
        
        
        """
        ...
    def createIdFromString(self, string: str) -> 'NameTypeIdGenerator.Id':
        """
            Splits the supplied string into its constituents and constructs an object from it
        
            Parameters:
                str (java.lang.String): the string to split
        
            Returns:
                a constructed :class:`~cern.accsoft.commons.domain.json.NameTypeIdGenerator.Id` object
        
            Raises:
                java.lang.IllegalArgumentException: if the supplied string is not of the format obj_name@obj_type
        
        
        """
        ...
    def forScope(self, class_: typing.Type[typing.Any]) -> 'NameTypeIdGenerator':
        """
        
            Specified by:
                :code:`forScope` in class :code:`com.fasterxml.jackson.annotation.ObjectIdGenerator<java.lang.String>`
        
        
        """
        ...
    def generateId(self, object: typing.Any) -> str:
        """
        
            Specified by:
                :code:`generateId` in class :code:`com.fasterxml.jackson.annotation.ObjectIdGenerator<java.lang.String>`
        
        
        """
        ...
    def getScope(self) -> typing.Type[typing.Any]:
        """
        
            Specified by:
                :code:`getScope` in class :code:`com.fasterxml.jackson.annotation.ObjectIdGenerator<java.lang.String>`
        
        
        """
        ...
    def key(self, object: typing.Any) -> com.fasterxml.jackson.annotation.ObjectIdGenerator.IdKey:
        """
        
            Specified by:
                :code:`key` in class :code:`com.fasterxml.jackson.annotation.ObjectIdGenerator<java.lang.String>`
        
        
        """
        ...
    def newForSerialization(self, object: typing.Any) -> com.fasterxml.jackson.annotation.ObjectIdGenerator[str]:
        """
        
            Specified by:
                :code:`newForSerialization` in class :code:`com.fasterxml.jackson.annotation.ObjectIdGenerator<java.lang.String>`
        
        
        """
        ...
    class Id:
        def getName(self) -> str: ...
        def getType(self) -> str: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.json")``.

    EnumEmulatorDeserializer: typing.Type[EnumEmulatorDeserializer]
    EnumEmulatorSerializer: typing.Type[EnumEmulatorSerializer]
    NameTypeIdGenerator: typing.Type[NameTypeIdGenerator]
    defaultobjects: cern.accsoft.commons.domain.json.defaultobjects.__module_protocol__
    modules: cern.accsoft.commons.domain.json.modules.__module_protocol__
