
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.beamdestinations
import cern.accsoft.commons.domain.beams
import cern.accsoft.commons.domain.json.modules
import cern.accsoft.commons.domain.lhc
import cern.accsoft.commons.domain.modes
import cern.accsoft.commons.domain.particletransfers
import cern.accsoft.commons.domain.zones
import com.fasterxml.jackson.databind.module
import java.util
import typing



class CernAcceleratorModeModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.modes.AcceleratorMode]):
    """
    public class CernAcceleratorModeModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.modes.AcceleratorMode>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernAcceleratorModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.Accelerator]):
    """
    public class CernAcceleratorModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.Accelerator>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernAcceleratorZoneModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.zones.AcceleratorZone]):
    """
    public class CernAcceleratorZoneModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.zones.AcceleratorZone>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernAccsoftCommonsDomainModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class CernAccsoftCommonsDomainModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...
    def getDependencies(self) -> java.util.List[com.fasterxml.jackson.databind.module.SimpleModule]:
        """
        
            Overrides:
                :code:`getDependencies` in class :code:`com.fasterxml.jackson.databind.Module`
        
        
        """
        ...

class CernBeamDestinationEndPointModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint]):
    """
    public class CernBeamDestinationEndPointModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernBeamDestinationModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.beamdestinations.BeamDestination]):
    """
    public class CernBeamDestinationModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.beamdestinations.BeamDestination>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernBeamModeModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.modes.BeamMode]):
    """
    public class CernBeamModeModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.modes.BeamMode>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernBeamModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.beams.Beam]):
    """
    public class CernBeamModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.beams.Beam>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernParticleTransferModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.particletransfers.ParticleTransfer]):
    """
    public class CernParticleTransferModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.particletransfers.ParticleTransfer>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class CernTimingDomainModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.TimingDomain]):
    """
    public class CernTimingDomainModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.TimingDomain>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...

class LhcInteractionPointModule(cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule[cern.accsoft.commons.domain.lhc.LhcInteractionPoint]):
    """
    public class LhcInteractionPointModule extends :class:`~cern.accsoft.commons.domain.json.modules.AbstractEnumEmulatorModule`<cern.accsoft.commons.domain.lhc.LhcInteractionPoint>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.json.modules.cern")``.

    CernAcceleratorModeModule: typing.Type[CernAcceleratorModeModule]
    CernAcceleratorModule: typing.Type[CernAcceleratorModule]
    CernAcceleratorZoneModule: typing.Type[CernAcceleratorZoneModule]
    CernAccsoftCommonsDomainModule: typing.Type[CernAccsoftCommonsDomainModule]
    CernBeamDestinationEndPointModule: typing.Type[CernBeamDestinationEndPointModule]
    CernBeamDestinationModule: typing.Type[CernBeamDestinationModule]
    CernBeamModeModule: typing.Type[CernBeamModeModule]
    CernBeamModule: typing.Type[CernBeamModule]
    CernParticleTransferModule: typing.Type[CernParticleTransferModule]
    CernTimingDomainModule: typing.Type[CernTimingDomainModule]
    LhcInteractionPointModule: typing.Type[LhcInteractionPointModule]
