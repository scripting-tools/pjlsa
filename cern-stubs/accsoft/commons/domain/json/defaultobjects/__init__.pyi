
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.beamdestinations
import cern.accsoft.commons.domain.beams
import cern.accsoft.commons.domain.modes
import cern.accsoft.commons.domain.particletransfers
import cern.accsoft.commons.domain.zones
import cern.accsoft.commons.util
import java.util
import typing



class UnknownAccelerator(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownAccelerator'], cern.accsoft.commons.domain.Accelerator):
    """
    public class UnknownAccelerator extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownAccelerator`> implements cern.accsoft.commons.domain.Accelerator
    
    
        Also see:
            :meth:`~serialized`
    """
    DEFAULT_UNKNOWN_ACCELERATOR: typing.ClassVar[cern.accsoft.commons.domain.Accelerator] = ...
    """
    public static final cern.accsoft.commons.domain.Accelerator DEFAULT_UNKNOWN_ACCELERATOR
    
    
    """
    @staticmethod
    def createNamedUnknownAccelerator(string: str) -> 'UnknownAccelerator': ...
    def getAcceleratorModes(self) -> java.util.Set[cern.accsoft.commons.domain.modes.AcceleratorMode]:
        """
        
            Specified by:
                :code:`getAcceleratorModes` in interface :code:`cern.accsoft.commons.domain.Accelerator`
        
        
        """
        ...
    def getAcceleratorZones(self) -> java.util.Set[cern.accsoft.commons.domain.zones.AcceleratorZone]:
        """
        
            Specified by:
                :code:`getAcceleratorZones` in interface :code:`cern.accsoft.commons.domain.Accelerator`
        
        
        """
        ...
    def getBeamDestinations(self) -> java.util.Set[cern.accsoft.commons.domain.beamdestinations.BeamDestination]:
        """
        
            Specified by:
                :code:`getBeamDestinations` in interface :code:`cern.accsoft.commons.domain.Accelerator`
        
        
        """
        ...
    def getBeams(self) -> java.util.Set[cern.accsoft.commons.domain.beams.Beam]:
        """
        
            Specified by:
                :code:`getBeams` in interface :code:`cern.accsoft.commons.domain.Accelerator`
        
        
        """
        ...
    def getCode(self) -> str: ...
    def getParticleTransfers(self) -> java.util.Set[cern.accsoft.commons.domain.particletransfers.ParticleTransfer]:
        """
        
            Specified by:
                :code:`getParticleTransfers` in interface :code:`cern.accsoft.commons.domain.Accelerator`
        
        
        """
        ...
    def getTimingDomain(self) -> cern.accsoft.commons.domain.TimingDomain:
        """
        
            Specified by:
                :code:`getTimingDomain` in interface :code:`cern.accsoft.commons.domain.Accelerator`
        
        
        """
        ...
    def isMultiplexed(self) -> bool:
        """
        
            Specified by:
                :code:`isMultiplexed` in interface :code:`cern.accsoft.commons.domain.Accelerator`
        
        
        """
        ...

class UnknownAcceleratorMode(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownAcceleratorMode'], cern.accsoft.commons.domain.modes.AcceleratorMode):
    """
    public class UnknownAcceleratorMode extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownAcceleratorMode`> implements cern.accsoft.commons.domain.modes.AcceleratorMode
    
    
        Also see:
            :meth:`~serialized`
    """
    @staticmethod
    def createNamedUnknownAcceleratorMode(string: str) -> 'UnknownAcceleratorMode': ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator:
        """
        
            Specified by:
                :code:`getAccelerator` in interface :code:`cern.accsoft.commons.domain.modes.AcceleratorMode`
        
        
        """
        ...
    def isOperational(self) -> bool:
        """
        
            Specified by:
                :code:`isOperational` in interface :code:`cern.accsoft.commons.domain.modes.AcceleratorMode`
        
        
        """
        ...

class UnknownAcceleratorZone(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownAcceleratorZone'], cern.accsoft.commons.domain.zones.AcceleratorZone):
    """
    public class UnknownAcceleratorZone extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownAcceleratorZone`> implements cern.accsoft.commons.domain.zones.AcceleratorZone
    
    
        Also see:
            :meth:`~serialized`
    """
    @staticmethod
    def createNamedUnknownAcceleratorZone(string: str) -> 'UnknownAcceleratorZone': ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator:
        """
        
            Specified by:
                :code:`getAccelerator` in interface :code:`cern.accsoft.commons.domain.zones.AcceleratorZone`
        
        
        """
        ...
    def getParticleTransfers(self) -> java.util.Set[cern.accsoft.commons.domain.particletransfers.ParticleTransfer]:
        """
        
            Specified by:
                :code:`getParticleTransfers` in interface :code:`cern.accsoft.commons.domain.zones.AcceleratorZone`
        
        
        """
        ...

class UnknownBeam(cern.accsoft.commons.util.AbstractImmutableNamedSerializable[cern.accsoft.commons.domain.beams.Beam], cern.accsoft.commons.domain.beams.Beam):
    """
    public class UnknownBeam extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<cern.accsoft.commons.domain.beams.Beam> implements cern.accsoft.commons.domain.beams.Beam
    
    
        Also see:
            :meth:`~serialized`
    """
    @staticmethod
    def createNamedUnknownBeam(string: str) -> 'UnknownBeam': ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator:
        """
        
            Specified by:
                :code:`getAccelerator` in interface :code:`cern.accsoft.commons.domain.beams.Beam`
        
        
        """
        ...
    def getBeamNumber(self) -> int:
        """
        
            Specified by:
                :code:`getBeamNumber` in interface :code:`cern.accsoft.commons.domain.beams.Beam`
        
        
        """
        ...

class UnknownBeamDestination(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownBeamDestination'], cern.accsoft.commons.domain.beamdestinations.BeamDestination):
    """
    public class UnknownBeamDestination extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownBeamDestination`> implements cern.accsoft.commons.domain.beamdestinations.BeamDestination
    
    
        Also see:
            :meth:`~serialized`
    """
    DEFAULT_UNKNOWN_BEAM_DESTINATION: typing.ClassVar[cern.accsoft.commons.domain.beamdestinations.BeamDestination] = ...
    """
    public static final cern.accsoft.commons.domain.beamdestinations.BeamDestination DEFAULT_UNKNOWN_BEAM_DESTINATION
    
    
    """
    @staticmethod
    def createNamedUnknownBeamDestination(string: str) -> 'UnknownBeamDestination': ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator:
        """
        
            Specified by:
                :code:`getAccelerator` in interface :code:`cern.accsoft.commons.domain.beamdestinations.BeamDestination`
        
        
        """
        ...
    def getEndPoints(self) -> java.util.Set[cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint]:
        """
        
            Specified by:
                :code:`getEndPoints` in interface :code:`cern.accsoft.commons.domain.beamdestinations.BeamDestination`
        
        
        """
        ...

class UnknownBeamDestinationEndPoint(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownBeamDestinationEndPoint'], cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint):
    """
    public class UnknownBeamDestinationEndPoint extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownBeamDestinationEndPoint`> implements cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint
    
    
        Also see:
            :meth:`~serialized`
    """
    @staticmethod
    def createNamedUnknownBeamDestinationEndPoint(string: str) -> 'UnknownBeamDestinationEndPoint': ...
    def getBeamDestination(self) -> cern.accsoft.commons.domain.beamdestinations.BeamDestination:
        """
        
            Specified by:
                :code:`getBeamDestination` in interface :code:`cern.accsoft.commons.domain.beamdestinations.BeamDestinationEndPoint`
        
        
        """
        ...

class UnknownBeamMode(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownBeamMode'], cern.accsoft.commons.domain.modes.BeamMode):
    """
    public class UnknownBeamMode extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownBeamMode`> implements cern.accsoft.commons.domain.modes.BeamMode
    
    
        Also see:
            :meth:`~serialized`
    """
    @staticmethod
    def createNamedUnknownBeamMode(string: str) -> 'UnknownBeamMode': ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator:
        """
        
            Specified by:
                :code:`getAccelerator` in interface :code:`cern.accsoft.commons.domain.modes.BeamMode`
        
        
        """
        ...

class UnknownParticleTransfer(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownParticleTransfer'], cern.accsoft.commons.domain.particletransfers.ParticleTransfer):
    """
    public class UnknownParticleTransfer extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownParticleTransfer`> implements cern.accsoft.commons.domain.particletransfers.ParticleTransfer
    
    
        Also see:
            :meth:`~serialized`
    """
    @staticmethod
    def createNamedUnknownParticleTransfer(string: str) -> 'UnknownParticleTransfer': ...
    def getAccelerator(self) -> cern.accsoft.commons.domain.Accelerator:
        """
        
            Specified by:
                :code:`getAccelerator` in interface :code:`cern.accsoft.commons.domain.particletransfers.ParticleTransfer`
        
        
        """
        ...
    def getAcceleratorZones(self) -> java.util.List[cern.accsoft.commons.domain.zones.AcceleratorZone]:
        """
        
            Specified by:
                :code:`getAcceleratorZones` in interface :code:`cern.accsoft.commons.domain.particletransfers.ParticleTransfer`
        
        
        """
        ...
    def getDescription(self) -> str:
        """
        
            Specified by:
                :code:`getDescription` in interface :code:`cern.accsoft.commons.domain.particletransfers.ParticleTransfer`
        
        
        """
        ...
    def getParticleTransferType(self) -> cern.accsoft.commons.domain.particletransfers.ParticleTransferType:
        """
        
            Specified by:
                :code:`getParticleTransferType` in interface :code:`cern.accsoft.commons.domain.particletransfers.ParticleTransfer`
        
        
        """
        ...

class UnknownParticleTransferType(cern.accsoft.commons.domain.particletransfers.ParticleTransferType):
    """
    public class UnknownParticleTransferType extends cern.accsoft.commons.domain.particletransfers.ParticleTransferType
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, string: str): ...
    @staticmethod
    def createNamedUnknownParticleTransferType(string: str) -> 'UnknownParticleTransferType': ...

class UnknownParticleType(cern.accsoft.commons.domain.ParticleType):
    """
    public class UnknownParticleType extends cern.accsoft.commons.domain.ParticleType
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, string: str): ...
    @staticmethod
    def createNamedUnknownParticleType(string: str) -> 'UnknownParticleType': ...

class UnknownTimingDomain(cern.accsoft.commons.util.AbstractImmutableNamedSerializable['UnknownTimingDomain'], cern.accsoft.commons.domain.TimingDomain):
    """
    public class UnknownTimingDomain extends cern.accsoft.commons.util.AbstractImmutableNamedSerializable<:class:`~cern.accsoft.commons.domain.json.defaultobjects.UnknownTimingDomain`> implements cern.accsoft.commons.domain.TimingDomain
    
    
        Also see:
            :meth:`~serialized`
    """
    DEFAULT_UNKNOWN_TIMING_DOMAIN: typing.ClassVar[cern.accsoft.commons.domain.TimingDomain] = ...
    """
    public static final cern.accsoft.commons.domain.TimingDomain DEFAULT_UNKNOWN_TIMING_DOMAIN
    
    
    """
    @staticmethod
    def createNamedUnknownTimingDomain(string: str) -> 'UnknownTimingDomain': ...
    def isCycling(self) -> bool:
        """
        
            Specified by:
                :code:`isCycling` in interface :code:`cern.accsoft.commons.domain.TimingDomain`
        
        
        """
        ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.json.defaultobjects")``.

    UnknownAccelerator: typing.Type[UnknownAccelerator]
    UnknownAcceleratorMode: typing.Type[UnknownAcceleratorMode]
    UnknownAcceleratorZone: typing.Type[UnknownAcceleratorZone]
    UnknownBeam: typing.Type[UnknownBeam]
    UnknownBeamDestination: typing.Type[UnknownBeamDestination]
    UnknownBeamDestinationEndPoint: typing.Type[UnknownBeamDestinationEndPoint]
    UnknownBeamMode: typing.Type[UnknownBeamMode]
    UnknownParticleTransfer: typing.Type[UnknownParticleTransfer]
    UnknownParticleTransferType: typing.Type[UnknownParticleTransferType]
    UnknownParticleType: typing.Type[UnknownParticleType]
    UnknownTimingDomain: typing.Type[UnknownTimingDomain]
