
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import typing



class LhcConstants:
    BENDING_RADIUS_LHC: typing.ClassVar[float] = ...
    MACHINE_CIRCUMFERENCE_LHC: typing.ClassVar[float] = ...
    MACHINE_RADIUS_LHC: typing.ClassVar[float] = ...
    GAMMA_TRANSITION_LHC: typing.ClassVar[float] = ...
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.constants.lhc")``.

    LhcConstants: typing.Type[LhcConstants]
