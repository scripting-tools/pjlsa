
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import typing



class PsConstants:
    PS_BENDING_RADIUS: typing.ClassVar[float] = ...
    PS_MACHINE_RADIUS: typing.ClassVar[float] = ...
    PS_MACHINE_CIRCUMNFERENCE: typing.ClassVar[float] = ...
    PS_GAMMA_TRANSITION: typing.ClassVar[float] = ...
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.constants.ps")``.

    PsConstants: typing.Type[PsConstants]
