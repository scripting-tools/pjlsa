
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import typing



class SpsConstants:
    SPS_BENDING_RADIUS: typing.ClassVar[float] = ...
    SPS_MACHINE_RADIUS: typing.ClassVar[float] = ...
    SPS_MACHINE_CIRCUMFERENCE: typing.ClassVar[float] = ...
    SPS_GAMMA_TRANSITION: typing.ClassVar[float] = ...
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.domain.constants.sps")``.

    SpsConstants: typing.Type[SpsConstants]
