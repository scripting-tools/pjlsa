
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.rest.core.annotations
import cern.accsoft.commons.rest.core.enums
import cern.accsoft.commons.rest.core.exceptions
import cern.accsoft.commons.rest.core.maps
import com.fasterxml.jackson.core
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.module
import java.util
import typing



class AccsoftCommonsRestModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class AccsoftCommonsRestModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...
    def getDependencies(self) -> java.util.List[com.fasterxml.jackson.databind.module.SimpleModule]:
        """
        
            Overrides:
                :code:`getDependencies` in class :code:`com.fasterxml.jackson.databind.Module`
        
        
        """
        ...

class AccsoftCommonsRestObjectMapper(com.fasterxml.jackson.databind.ObjectMapper):
    """
    public class AccsoftCommonsRestObjectMapper extends com.fasterxml.jackson.databind.ObjectMapper
    
    
        Also see:
            :meth:`~serialized`
    """
    @staticmethod
    def createInstance() -> 'AccsoftCommonsRestObjectMapper': ...
    @typing.overload
    def disable(self, deserializationFeature: com.fasterxml.jackson.databind.DeserializationFeature) -> 'AccsoftCommonsRestObjectMapper':
        """
        
            Overrides:
                :code:`disable` in class :code:`com.fasterxml.jackson.databind.ObjectMapper`
        
        
            Overrides:
                :code:`disable` in class :code:`com.fasterxml.jackson.databind.ObjectMapper`
        
        
        """
        ...
    @typing.overload
    def disable(self, serializationFeature: com.fasterxml.jackson.databind.SerializationFeature) -> 'AccsoftCommonsRestObjectMapper': ...
    @typing.overload
    def disable(self, *feature: com.fasterxml.jackson.core.JsonGenerator.Feature) -> com.fasterxml.jackson.databind.ObjectMapper: ...
    @typing.overload
    def disable(self, *feature: com.fasterxml.jackson.core.JsonParser.Feature) -> com.fasterxml.jackson.databind.ObjectMapper: ...
    @typing.overload
    def disable(self, deserializationFeature: com.fasterxml.jackson.databind.DeserializationFeature, *deserializationFeature2: com.fasterxml.jackson.databind.DeserializationFeature) -> com.fasterxml.jackson.databind.ObjectMapper: ...
    @typing.overload
    def disable(self, *mapperFeature: com.fasterxml.jackson.databind.MapperFeature) -> com.fasterxml.jackson.databind.ObjectMapper: ...
    @typing.overload
    def disable(self, serializationFeature: com.fasterxml.jackson.databind.SerializationFeature, *serializationFeature2: com.fasterxml.jackson.databind.SerializationFeature) -> com.fasterxml.jackson.databind.ObjectMapper: ...
    def registerModule(self, module: com.fasterxml.jackson.databind.Module) -> 'AccsoftCommonsRestObjectMapper':
        """
        
            Overrides:
                :code:`registerModule` in class :code:`com.fasterxml.jackson.databind.ObjectMapper`
        
        
        """
        ...

class CommonRestUrls:
    """
    public final class CommonRestUrls extends java.lang.Object
    """
    BY: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BY_NAME: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BY_NAME
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    BY_ID: typing.ClassVar[str] = ...
    """
    public static final java.lang.String BY_ID
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DO: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DO
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DO_SEARCH: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DO_SEARCH
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DO_GET: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DO_GET
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DO_GROUP_BY: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DO_GROUP_BY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DO_MAP_BY: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DO_MAP_BY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    GET: typing.ClassVar[str] = ...
    """
    public static final java.lang.String GET
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    POST: typing.ClassVar[str] = ...
    """
    public static final java.lang.String POST
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    PUT: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PUT
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    PATCH: typing.ClassVar[str] = ...
    """
    public static final java.lang.String PATCH
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    DELETE: typing.ClassVar[str] = ...
    """
    public static final java.lang.String DELETE
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    CONTENT_TYPE_APPLICATION_JSON_UTF_8: typing.ClassVar[str] = ...
    """
    public static final java.lang.String CONTENT_TYPE_APPLICATION_JSON_UTF_8
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ACCEPT_APPLICATION_JSON: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ACCEPT_APPLICATION_JSON
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    ACCEPT_CHARSET_UTF_8: typing.ClassVar[str] = ...
    """
    public static final java.lang.String ACCEPT_CHARSET_UTF_8
    
    
        Also see:
            :meth:`~constant`
    
    
    """


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.rest.core")``.

    AccsoftCommonsRestModule: typing.Type[AccsoftCommonsRestModule]
    AccsoftCommonsRestObjectMapper: typing.Type[AccsoftCommonsRestObjectMapper]
    CommonRestUrls: typing.Type[CommonRestUrls]
    annotations: cern.accsoft.commons.rest.core.annotations.__module_protocol__
    enums: cern.accsoft.commons.rest.core.enums.__module_protocol__
    exceptions: cern.accsoft.commons.rest.core.exceptions.__module_protocol__
    maps: cern.accsoft.commons.rest.core.maps.__module_protocol__
