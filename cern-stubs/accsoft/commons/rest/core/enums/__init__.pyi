
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.fasterxml.jackson.core
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.deser
import com.fasterxml.jackson.databind.deser.std
import com.fasterxml.jackson.databind.jsontype
import com.fasterxml.jackson.databind.module
import com.fasterxml.jackson.databind.ser
import java.lang
import typing



class EnumDeserializerModifier(com.fasterxml.jackson.databind.deser.BeanDeserializerModifier):
    """
    public class EnumDeserializerModifier extends com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
    """
    def __init__(self): ...
    def modifyEnumDeserializer(self, deserializationConfig: com.fasterxml.jackson.databind.DeserializationConfig, javaType: com.fasterxml.jackson.databind.JavaType, beanDescription: com.fasterxml.jackson.databind.BeanDescription, jsonDeserializer: com.fasterxml.jackson.databind.JsonDeserializer[typing.Any]) -> com.fasterxml.jackson.databind.JsonDeserializer[typing.Any]:
        """
        
            Overrides:
                :code:`modifyEnumDeserializer` in class :code:`com.fasterxml.jackson.databind.deser.BeanDeserializerModifier`
        
        
        """
        ...

_EnumFromObjectDeserializer__T = typing.TypeVar('_EnumFromObjectDeserializer__T', bound=java.lang.Enum)  # <T>
class EnumFromObjectDeserializer(com.fasterxml.jackson.databind.deser.std.StdDeserializer[_EnumFromObjectDeserializer__T], typing.Generic[_EnumFromObjectDeserializer__T]):
    """
    public class EnumFromObjectDeserializer<T extends java.lang.Enum<T> & cern.accsoft.commons.util.Named> extends com.fasterxml.jackson.databind.deser.std.StdDeserializer<T>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, class_: typing.Type[_EnumFromObjectDeserializer__T]): ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext) -> _EnumFromObjectDeserializer__T: ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, t: _EnumFromObjectDeserializer__T) -> _EnumFromObjectDeserializer__T: ...
    @typing.overload
    def deserializeWithType(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, typeDeserializer: com.fasterxml.jackson.databind.jsontype.TypeDeserializer) -> typing.Any: ...
    @typing.overload
    def deserializeWithType(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, typeDeserializer: com.fasterxml.jackson.databind.jsontype.TypeDeserializer, t2: _EnumFromObjectDeserializer__T) -> typing.Any: ...

class EnumSerializationException(java.lang.IllegalArgumentException):
    """
    public class EnumSerializationException extends java.lang.IllegalArgumentException
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, string: str): ...

class EnumSerializationValidator(com.fasterxml.jackson.databind.ser.BeanSerializerModifier):
    """
    public class EnumSerializationValidator extends com.fasterxml.jackson.databind.ser.BeanSerializerModifier
    """
    def __init__(self): ...
    def modifySerializer(self, serializationConfig: com.fasterxml.jackson.databind.SerializationConfig, beanDescription: com.fasterxml.jackson.databind.BeanDescription, jsonSerializer: com.fasterxml.jackson.databind.JsonSerializer[typing.Any]) -> com.fasterxml.jackson.databind.JsonSerializer[typing.Any]:
        """
        
            Overrides:
                :code:`modifySerializer` in class :code:`com.fasterxml.jackson.databind.ser.BeanSerializerModifier`
        
        
        """
        ...

class EnumsInJsonEnforcer:
    """
    public final class EnumsInJsonEnforcer extends java.lang.Object
    """
    @staticmethod
    def assertIsJsonable(class_: typing.Type[java.lang.Enum[typing.Any]], beanDescription: com.fasterxml.jackson.databind.BeanDescription) -> None:
        """
            Asserts that an Enum class implements :code:`Named` and that the names are unique for each Enum element
        
            Parameters:
                clazz (java.lang.Class<? extends java.lang.Enum<?>>): the Enum class to assert
                beanDescription (com.fasterxml.jackson.databind.BeanDescription): the bean description in order to check the annotations. Required to work with MixIns
        
        
        """
        ...
    @staticmethod
    def shouldBeSerializedAsObject(beanDescription: com.fasterxml.jackson.databind.BeanDescription) -> bool: ...

class EnumsSimpleModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class EnumsSimpleModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.rest.core.enums")``.

    EnumDeserializerModifier: typing.Type[EnumDeserializerModifier]
    EnumFromObjectDeserializer: typing.Type[EnumFromObjectDeserializer]
    EnumSerializationException: typing.Type[EnumSerializationException]
    EnumSerializationValidator: typing.Type[EnumSerializationValidator]
    EnumsInJsonEnforcer: typing.Type[EnumsInJsonEnforcer]
    EnumsSimpleModule: typing.Type[EnumsSimpleModule]
