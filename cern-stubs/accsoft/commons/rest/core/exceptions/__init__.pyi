
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.fasterxml.jackson.annotation
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.jsontype.impl
import com.fasterxml.jackson.databind.module
import com.fasterxml.jackson.databind.util
import java.lang
import typing



class ExceptionSnapshot(java.lang.RuntimeException):
    """
    public class ExceptionSnapshot extends java.lang.RuntimeException
    
    
        Also see:
            :meth:`~serialized`
    """
    @typing.overload
    def __init__(self, string: str, throwable: java.lang.Throwable, string2: str): ...
    @typing.overload
    def __init__(self, throwable: java.lang.Throwable): ...
    def getOriginalExceptionClassname(self) -> str: ...
    def toString(self) -> str:
        """
        
            Overrides:
                :code:`toString` in class :code:`java.lang.Throwable`
        
        
        """
        ...

class ExceptionsModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class ExceptionsModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...
    @staticmethod
    def createExceptionSnapshotMappingModule(class_: typing.Type[java.lang.Throwable]) -> com.fasterxml.jackson.databind.module.SimpleModule: ...
    def getDependencies(self) -> java.lang.Iterable[com.fasterxml.jackson.databind.Module]:
        """
        
            Overrides:
                :code:`getDependencies` in class :code:`com.fasterxml.jackson.databind.Module`
        
        
        """
        ...

class ThrowableMixIn:
    """
    public interface ThrowableMixIn
    """
    CLASSNAME_PROPERTY: typing.ClassVar[str] = ...
    """
    static final java.lang.String CLASSNAME_PROPERTY
    
    
        Also see:
            :meth:`~constant`
    
    
    """
    def getLocalizedMessage(self) -> str: ...

class ThrowableTypeIdResolver(com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase):
    """
    public class ThrowableTypeIdResolver extends com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase
    """
    def __init__(self): ...
    def getMechanism(self) -> com.fasterxml.jackson.annotation.JsonTypeInfo.Id: ...
    def idFromValue(self, object: typing.Any) -> str: ...
    def idFromValueAndType(self, object: typing.Any, class_: typing.Type[typing.Any]) -> str: ...
    def init(self, javaType: com.fasterxml.jackson.databind.JavaType) -> None:
        """
        
            Specified by:
                :code:`init` in interface :code:`com.fasterxml.jackson.databind.jsontype.TypeIdResolver`
        
            Overrides:
                :code:`init` in class :code:`com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase`
        
        
        """
        ...
    def typeFromId(self, databindContext: com.fasterxml.jackson.databind.DatabindContext, string: str) -> com.fasterxml.jackson.databind.JavaType:
        """
        
            Specified by:
                :code:`typeFromId` in interface :code:`com.fasterxml.jackson.databind.jsontype.TypeIdResolver`
        
            Overrides:
                :code:`typeFromId` in class :code:`com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase`
        
        
        """
        ...

class ToExceptionSnapshotConverter(com.fasterxml.jackson.databind.util.StdConverter[java.lang.Throwable, ExceptionSnapshot]):
    """
    public class ToExceptionSnapshotConverter extends com.fasterxml.jackson.databind.util.StdConverter<java.lang.Throwable, :class:`~cern.accsoft.commons.rest.core.exceptions.ExceptionSnapshot`>
    """
    def __init__(self): ...
    def convert(self, throwable: java.lang.Throwable) -> ExceptionSnapshot:
        """
        
            Specified by:
                :code:`convert` in interface :class:`~cern.accsoft.commons.rest.core.exceptions.ExceptionSnapshot`
        
            Specified by:
                :code:`convert` in class :class:`~cern.accsoft.commons.rest.core.exceptions.ExceptionSnapshot`
        
        
        """
        ...

class ToExceptionSnapshotMixIn:
    """
    public interface ToExceptionSnapshotMixIn
    """
    ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.rest.core.exceptions")``.

    ExceptionSnapshot: typing.Type[ExceptionSnapshot]
    ExceptionsModule: typing.Type[ExceptionsModule]
    ThrowableMixIn: typing.Type[ThrowableMixIn]
    ThrowableTypeIdResolver: typing.Type[ThrowableTypeIdResolver]
    ToExceptionSnapshotConverter: typing.Type[ToExceptionSnapshotConverter]
    ToExceptionSnapshotMixIn: typing.Type[ToExceptionSnapshotMixIn]
