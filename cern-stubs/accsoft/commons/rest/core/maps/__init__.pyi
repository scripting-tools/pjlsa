
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.fasterxml.jackson.core
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.deser
import com.fasterxml.jackson.databind.deser.std
import com.fasterxml.jackson.databind.module
import com.fasterxml.jackson.databind.ser
import com.fasterxml.jackson.databind.ser.std
import com.fasterxml.jackson.databind.type
import java.util
import typing



class GenericMapKeyChecker:
    """
    public class GenericMapKeyChecker extends java.lang.Object
    """
    ...

class MapAsArraySerializer(com.fasterxml.jackson.databind.ser.std.StdSerializer[java.util.Map[typing.Any, typing.Any]]):
    """
    public class MapAsArraySerializer extends com.fasterxml.jackson.databind.ser.std.StdSerializer<java.util.Map<?, ?>>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, mapType: com.fasterxml.jackson.databind.type.MapType): ...
    def serialize(self, map: typing.Union[java.util.Map[typing.Any, typing.Any], typing.Mapping[typing.Any, typing.Any]], jsonGenerator: com.fasterxml.jackson.core.JsonGenerator, serializerProvider: com.fasterxml.jackson.databind.SerializerProvider) -> None: ...

class MapDeserializerModifier(com.fasterxml.jackson.databind.deser.BeanDeserializerModifier):
    """
    public class MapDeserializerModifier extends com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
    """
    def __init__(self): ...
    def modifyMapDeserializer(self, deserializationConfig: com.fasterxml.jackson.databind.DeserializationConfig, mapType: com.fasterxml.jackson.databind.type.MapType, beanDescription: com.fasterxml.jackson.databind.BeanDescription, jsonDeserializer: com.fasterxml.jackson.databind.JsonDeserializer[typing.Any]) -> com.fasterxml.jackson.databind.JsonDeserializer[typing.Any]:
        """
        
            Overrides:
                :code:`modifyMapDeserializer` in class :code:`com.fasterxml.jackson.databind.deser.BeanDeserializerModifier`
        
        
        """
        ...

_MapFromArrayDeserializer__T = typing.TypeVar('_MapFromArrayDeserializer__T', bound=java.util.Map)  # <T>
class MapFromArrayDeserializer(com.fasterxml.jackson.databind.deser.std.StdDeserializer[_MapFromArrayDeserializer__T], typing.Generic[_MapFromArrayDeserializer__T]):
    """
    public class MapFromArrayDeserializer<T extends java.util.Map<java.lang.Object, java.lang.Object>> extends com.fasterxml.jackson.databind.deser.std.StdDeserializer<T>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, javaType: com.fasterxml.jackson.databind.JavaType): ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, t: _MapFromArrayDeserializer__T) -> _MapFromArrayDeserializer__T: ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext) -> _MapFromArrayDeserializer__T: ...

class MapSerializerModifier(com.fasterxml.jackson.databind.ser.BeanSerializerModifier):
    """
    public class MapSerializerModifier extends com.fasterxml.jackson.databind.ser.BeanSerializerModifier
    """
    def __init__(self): ...
    def modifyMapSerializer(self, serializationConfig: com.fasterxml.jackson.databind.SerializationConfig, mapType: com.fasterxml.jackson.databind.type.MapType, beanDescription: com.fasterxml.jackson.databind.BeanDescription, jsonSerializer: com.fasterxml.jackson.databind.JsonSerializer[typing.Any]) -> com.fasterxml.jackson.databind.JsonSerializer[typing.Any]:
        """
        
            Overrides:
                :code:`modifyMapSerializer` in class :code:`com.fasterxml.jackson.databind.ser.BeanSerializerModifier`
        
        
        """
        ...

class MapSimpleModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class MapSimpleModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.rest.core.maps")``.

    GenericMapKeyChecker: typing.Type[GenericMapKeyChecker]
    MapAsArraySerializer: typing.Type[MapAsArraySerializer]
    MapDeserializerModifier: typing.Type[MapDeserializerModifier]
    MapFromArrayDeserializer: typing.Type[MapFromArrayDeserializer]
    MapSerializerModifier: typing.Type[MapSerializerModifier]
    MapSimpleModule: typing.Type[MapSimpleModule]
