
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.fasterxml.jackson.core
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.module
import com.fasterxml.jackson.databind.ser
import com.fasterxml.jackson.databind.ser.std
import com.fasterxml.jackson.databind.type
import typing



class CollectionLikePerItemSerializer(com.fasterxml.jackson.databind.ser.std.StdSerializer[typing.Any]):
    """
    public class CollectionLikePerItemSerializer extends com.fasterxml.jackson.databind.ser.std.StdSerializer<java.lang.Object>
    
    
        Also see:
            :meth:`~serialized`
    """
    def serialize(self, object: typing.Any, jsonGenerator: com.fasterxml.jackson.core.JsonGenerator, serializerProvider: com.fasterxml.jackson.databind.SerializerProvider) -> None: ...

class CollectionLikeSerializerModifier(com.fasterxml.jackson.databind.ser.BeanSerializerModifier):
    """
    public class CollectionLikeSerializerModifier extends com.fasterxml.jackson.databind.ser.BeanSerializerModifier
    """
    def __init__(self): ...
    def modifyArraySerializer(self, serializationConfig: com.fasterxml.jackson.databind.SerializationConfig, arrayType: com.fasterxml.jackson.databind.type.ArrayType, beanDescription: com.fasterxml.jackson.databind.BeanDescription, jsonSerializer: com.fasterxml.jackson.databind.JsonSerializer[typing.Any]) -> com.fasterxml.jackson.databind.JsonSerializer[typing.Any]:
        """
        
            Overrides:
                :code:`modifyArraySerializer` in class :code:`com.fasterxml.jackson.databind.ser.BeanSerializerModifier`
        
        
        """
        ...
    def modifyCollectionSerializer(self, serializationConfig: com.fasterxml.jackson.databind.SerializationConfig, collectionType: com.fasterxml.jackson.databind.type.CollectionType, beanDescription: com.fasterxml.jackson.databind.BeanDescription, jsonSerializer: com.fasterxml.jackson.databind.JsonSerializer[typing.Any]) -> com.fasterxml.jackson.databind.JsonSerializer[typing.Any]:
        """
        
            Overrides:
                :code:`modifyCollectionSerializer` in class :code:`com.fasterxml.jackson.databind.ser.BeanSerializerModifier`
        
        
        """
        ...

class CollectionLikeSimpleModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class CollectionLikeSimpleModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.rest.core.collections")``.

    CollectionLikePerItemSerializer: typing.Type[CollectionLikePerItemSerializer]
    CollectionLikeSerializerModifier: typing.Type[CollectionLikeSerializerModifier]
    CollectionLikeSimpleModule: typing.Type[CollectionLikeSimpleModule]
