
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.domain
import cern.accsoft.commons.domain.modes
import cern.accsoft.commons.domain.util
import typing



class LhcAcceleratorModeConverter(cern.accsoft.commons.domain.util.CodeEntityConverter[cern.accsoft.commons.domain.modes.LhcAcceleratorMode]):
    LHC_ACCELERATOR_MODE_CONVERTER: typing.ClassVar['LhcAcceleratorModeConverter'] = ...

class LhcBeamModeConverter(cern.accsoft.commons.domain.util.CodeEntityConverter[cern.accsoft.commons.domain.modes.LhcBeamMode]):
    LHC_BEAM_MODE_CONVERTER: typing.ClassVar['LhcBeamModeConverter'] = ...

class LhcParticleTypeConverter(cern.accsoft.commons.domain.util.CodeEntityConverter[cern.accsoft.commons.domain.ParticleType]):
    LHC_PARTICLE_TYPE_CONVERTER: typing.ClassVar['LhcParticleTypeConverter'] = ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.timing")``.

    LhcAcceleratorModeConverter: typing.Type[LhcAcceleratorModeConverter]
    LhcBeamModeConverter: typing.Type[LhcBeamModeConverter]
    LhcParticleTypeConverter: typing.Type[LhcParticleTypeConverter]
