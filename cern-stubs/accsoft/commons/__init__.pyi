
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.ccs
import cern.accsoft.commons.diag
import cern.accsoft.commons.domain
import cern.accsoft.commons.rest
import cern.accsoft.commons.timing
import cern.accsoft.commons.util
import cern.accsoft.commons.value
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons")``.

    ccs: cern.accsoft.commons.ccs.__module_protocol__
    diag: cern.accsoft.commons.diag.__module_protocol__
    domain: cern.accsoft.commons.domain.__module_protocol__
    rest: cern.accsoft.commons.rest.__module_protocol__
    timing: cern.accsoft.commons.timing.__module_protocol__
    util: cern.accsoft.commons.util.__module_protocol__
    value: cern.accsoft.commons.value.__module_protocol__
