
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.array
import cern.accsoft.commons.value.json.array2d
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.descriptor
import cern.accsoft.commons.value.json.function
import cern.accsoft.commons.value.json.scalar
import com.fasterxml.jackson.core
import com.fasterxml.jackson.databind
import com.fasterxml.jackson.databind.deser.std
import com.fasterxml.jackson.databind.module
import com.fasterxml.jackson.databind.ser.std
import java.util
import typing



class CernAccsoftCommonsValueModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class CernAccsoftCommonsValueModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...
    def getDependencies(self) -> java.util.List[com.fasterxml.jackson.databind.module.SimpleModule]:
        """
        
            Overrides:
                :code:`getDependencies` in class :code:`com.fasterxml.jackson.databind.Module`
        
        
        """
        ...

_TypedDeserializer__DOMAIN = typing.TypeVar('_TypedDeserializer__DOMAIN', bound=cern.accsoft.commons.value.Typed)  # <DOMAIN>
_TypedDeserializer__DTO = typing.TypeVar('_TypedDeserializer__DTO', bound=cern.accsoft.commons.value.json.common.TypedDto)  # <DTO>
class TypedDeserializer(com.fasterxml.jackson.databind.deser.std.StdDeserializer[_TypedDeserializer__DOMAIN], typing.Generic[_TypedDeserializer__DOMAIN, _TypedDeserializer__DTO]):
    """
    public class TypedDeserializer<DOMAIN extends cern.accsoft.commons.value.Typed, DTO extends :class:`~cern.accsoft.commons.value.json.common.TypedDto`<DOMAIN>> extends com.fasterxml.jackson.databind.deser.std.StdDeserializer<DOMAIN>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, class_: typing.Type[_TypedDeserializer__DOMAIN], class2: typing.Type[_TypedDeserializer__DTO]): ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext) -> _TypedDeserializer__DOMAIN: ...
    @typing.overload
    def deserialize(self, jsonParser: com.fasterxml.jackson.core.JsonParser, deserializationContext: com.fasterxml.jackson.databind.DeserializationContext, t: typing.Any) -> typing.Any: ...

class TypedSerializer(com.fasterxml.jackson.databind.ser.std.StdSerializer[cern.accsoft.commons.value.Typed]):
    """
    public class TypedSerializer extends com.fasterxml.jackson.databind.ser.std.StdSerializer<cern.accsoft.commons.value.Typed>
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self, toTypedDto: cern.accsoft.commons.value.json.common.ToTypedDto): ...
    def serialize(self, typed: cern.accsoft.commons.value.Typed, jsonGenerator: com.fasterxml.jackson.core.JsonGenerator, serializerProvider: com.fasterxml.jackson.databind.SerializerProvider) -> None: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json")``.

    CernAccsoftCommonsValueModule: typing.Type[CernAccsoftCommonsValueModule]
    TypedDeserializer: typing.Type[TypedDeserializer]
    TypedSerializer: typing.Type[TypedSerializer]
    array: cern.accsoft.commons.value.json.array.__module_protocol__
    array2d: cern.accsoft.commons.value.json.array2d.__module_protocol__
    common: cern.accsoft.commons.value.json.common.__module_protocol__
    descriptor: cern.accsoft.commons.value.json.descriptor.__module_protocol__
    function: cern.accsoft.commons.value.json.function.__module_protocol__
    scalar: cern.accsoft.commons.value.json.scalar.__module_protocol__
