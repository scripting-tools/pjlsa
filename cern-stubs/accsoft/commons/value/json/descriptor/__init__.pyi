
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.fasterxml.jackson.databind.module
import typing



class ValueDescriptorModule(com.fasterxml.jackson.databind.module.SimpleModule):
    """
    public class ValueDescriptorModule extends com.fasterxml.jackson.databind.module.SimpleModule
    
    
        Also see:
            :meth:`~serialized`
    """
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.descriptor")``.

    ValueDescriptorModule: typing.Type[ValueDescriptorModule]
