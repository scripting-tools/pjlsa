
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import typing



class PointData:
    """
    public class PointData extends java.lang.Object
    """
    def __init__(self, double: float, double2: float): ...
    def getX(self) -> float: ...
    def getY(self) -> float: ...

_ScalarDto__D = typing.TypeVar('_ScalarDto__D', bound=cern.accsoft.commons.value.Scalar)  # <D>
_ScalarDto__R = typing.TypeVar('_ScalarDto__R')  # <R>
class ScalarDto(cern.accsoft.commons.value.json.common.ValueDto[_ScalarDto__D, _ScalarDto__R], typing.Generic[_ScalarDto__D, _ScalarDto__R]):
    """
    public abstract class ScalarDto<D extends cern.accsoft.commons.value.Scalar, R> extends :class:`~cern.accsoft.commons.value.json.common.ValueDto`<D, R>
    """
    ...

class ToScalarDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.ImmutableScalar, 'ScalarDtoAsScalar'[typing.Any]]):
    """
    public class ToScalarDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.ImmutableScalar, cern.accsoft.commons.value.json.scalar.ScalarDtoAsScalar<?>>
    """
    def __init__(self): ...
    def convert(self, immutableScalar: cern.accsoft.commons.value.ImmutableScalar) -> 'ScalarDtoAsScalar'[typing.Any]:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class ScalarDtoAsScalar: ...

class BooleanDto(ScalarDtoAsScalar[bool]):
    """
    public class BooleanDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, boolean: bool): ...

class ByteDto(ScalarDtoAsScalar[int]):
    """
    public class ByteDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, byte: int): ...

class DoubleDto(ScalarDtoAsScalar[float]):
    """
    public class DoubleDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, double: float): ...

class EnumDto(ScalarDtoAsScalar[str]):
    """
    public class EnumDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, string: str): ...

class FloatDto(ScalarDtoAsScalar[float]):
    """
    public class FloatDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, float: float): ...

class IntDto(ScalarDtoAsScalar[int]):
    """
    public class IntDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, int: int): ...

class LongDto(ScalarDtoAsScalar[int]):
    """
    public class LongDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, long: int): ...

class PointDto(ScalarDtoAsScalar[PointData]):
    """
    public class PointDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, pointData: PointData): ...

class ShortDto(ScalarDtoAsScalar[int]):
    """
    public class ShortDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, short: int): ...

class StringDto(ScalarDtoAsScalar[str]):
    """
    public class StringDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, string: str): ...

class TextDocumentDto(ScalarDtoAsScalar[str]):
    """
    public class TextDocumentDto extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<cern.accsoft.commons.value.Scalar, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, string: str): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.scalar")``.

    BooleanDto: typing.Type[BooleanDto]
    ByteDto: typing.Type[ByteDto]
    DoubleDto: typing.Type[DoubleDto]
    EnumDto: typing.Type[EnumDto]
    FloatDto: typing.Type[FloatDto]
    IntDto: typing.Type[IntDto]
    LongDto: typing.Type[LongDto]
    PointData: typing.Type[PointData]
    PointDto: typing.Type[PointDto]
    ScalarDto: typing.Type[ScalarDto]
    ScalarDtoAsScalar: typing.Type[ScalarDtoAsScalar]
    ShortDto: typing.Type[ShortDto]
    StringDto: typing.Type[StringDto]
    TextDocumentDto: typing.Type[TextDocumentDto]
    ToScalarDto: typing.Type[ToScalarDto]
