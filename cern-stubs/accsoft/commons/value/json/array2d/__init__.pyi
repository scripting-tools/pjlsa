
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.array
import cern.accsoft.commons.value.json.common
import jpype
import typing



_Array2DDto__R = typing.TypeVar('_Array2DDto__R')  # <R>
class Array2DDto(cern.accsoft.commons.value.json.array.ArrayDto[cern.accsoft.commons.value.ScalarArray2D, _Array2DDto__R], typing.Generic[_Array2DDto__R]):
    """
    public abstract class Array2DDto<R> extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray2D, R>
    """
    def getColumnCount(self) -> int: ...
    def getRowCount(self) -> int: ...

class ToArray2DDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.ImmutableScalarArray2D, Array2DDto[typing.Any]]):
    """
    public class ToArray2DDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.ImmutableScalarArray2D, :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<?>>
    """
    def __init__(self): ...
    def convert(self, immutableScalarArray2D: cern.accsoft.commons.value.ImmutableScalarArray2D) -> Array2DDto[typing.Any]:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class BooleanArray2DDto(Array2DDto[typing.MutableSequence[bool]]):
    """
    public class BooleanArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<boolean[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, booleanArray: typing.Union[typing.List[bool], jpype.JArray], int: int, int2: int): ...

class ByteArray2DDto(Array2DDto[typing.MutableSequence[int]]):
    """
    public class ByteArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<short[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, shortArray: typing.Union[typing.List[int], jpype.JArray], int: int, int2: int): ...

class DoubleArray2DDto(Array2DDto[typing.MutableSequence[float]]):
    """
    public class DoubleArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<double[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, doubleArray: typing.Union[typing.List[float], jpype.JArray], int: int, int2: int): ...

class FloatArray2DDto(Array2DDto[typing.MutableSequence[float]]):
    """
    public class FloatArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<float[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, floatArray: typing.Union[typing.List[float], jpype.JArray], int: int, int2: int): ...

class IntArray2DDto(Array2DDto[typing.MutableSequence[int]]):
    """
    public class IntArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<int[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, intArray: typing.Union[typing.List[int], jpype.JArray], int2: int, int3: int): ...

class LongArray2DDto(Array2DDto[typing.MutableSequence[int]]):
    """
    public class LongArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<long[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, longArray: typing.Union[typing.List[int], jpype.JArray], int: int, int2: int): ...

class ShortArray2DDto(Array2DDto[typing.MutableSequence[int]]):
    """
    public class ShortArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<short[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, shortArray: typing.Union[typing.List[int], jpype.JArray], int: int, int2: int): ...

class StringArray2DDto(Array2DDto[typing.MutableSequence[str]]):
    """
    public class StringArray2DDto extends :class:`~cern.accsoft.commons.value.json.array2d.Array2DDto`<java.lang.String[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, stringArray: typing.Union[typing.List[str], jpype.JArray], int: int, int2: int): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.array2d")``.

    Array2DDto: typing.Type[Array2DDto]
    BooleanArray2DDto: typing.Type[BooleanArray2DDto]
    ByteArray2DDto: typing.Type[ByteArray2DDto]
    DoubleArray2DDto: typing.Type[DoubleArray2DDto]
    FloatArray2DDto: typing.Type[FloatArray2DDto]
    IntArray2DDto: typing.Type[IntArray2DDto]
    LongArray2DDto: typing.Type[LongArray2DDto]
    ShortArray2DDto: typing.Type[ShortArray2DDto]
    StringArray2DDto: typing.Type[StringArray2DDto]
    ToArray2DDto: typing.Type[ToArray2DDto]
