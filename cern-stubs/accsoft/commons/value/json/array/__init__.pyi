
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.scalar
import jpype
import typing



_ArrayDto__D = typing.TypeVar('_ArrayDto__D', bound=cern.accsoft.commons.value.ScalarArray)  # <D>
_ArrayDto__R = typing.TypeVar('_ArrayDto__R')  # <R>
class ArrayDto(cern.accsoft.commons.value.json.scalar.ScalarDto[_ArrayDto__D, _ArrayDto__R], typing.Generic[_ArrayDto__D, _ArrayDto__R]):
    """
    public abstract class ArrayDto<D extends cern.accsoft.commons.value.ScalarArray, R> extends :class:`~cern.accsoft.commons.value.json.scalar.ScalarDto`<D, R>
    """
    ...

class ToArrayDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.ImmutableScalarArray, 'ArrayDtoAsScalarArray'[typing.Any]]):
    """
    public class ToArrayDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.ImmutableScalarArray, cern.accsoft.commons.value.json.array.ArrayDtoAsScalarArray<?>>
    """
    def __init__(self): ...
    def convert(self, immutableScalarArray: cern.accsoft.commons.value.ImmutableScalarArray) -> 'ArrayDtoAsScalarArray'[typing.Any]:
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class ArrayDtoAsScalarArray: ...

class BooleanArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[bool]]):
    """
    public class BooleanArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, booleanArray: typing.Union[typing.List[bool], jpype.JArray]): ...

class ByteArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[int]]):
    """
    public class ByteArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, shortArray: typing.Union[typing.List[int], jpype.JArray]): ...

class DoubleArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[float]]):
    """
    public class DoubleArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, doubleArray: typing.Union[typing.List[float], jpype.JArray]): ...

class FloatArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[float]]):
    """
    public class FloatArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, floatArray: typing.Union[typing.List[float], jpype.JArray]): ...

class IntArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[int]]):
    """
    public class IntArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, intArray: typing.Union[typing.List[int], jpype.JArray]): ...

class LongArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[int]]):
    """
    public class LongArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, longArray: typing.Union[typing.List[int], jpype.JArray]): ...

class ShortArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[int]]):
    """
    public class ShortArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, shortArray: typing.Union[typing.List[int], jpype.JArray]): ...

class StringArrayDto(ArrayDtoAsScalarArray[typing.MutableSequence[str]]):
    """
    public class StringArrayDto extends :class:`~cern.accsoft.commons.value.json.array.ArrayDto`<cern.accsoft.commons.value.ScalarArray, R>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, stringArray: typing.Union[typing.List[str], jpype.JArray]): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.array")``.

    ArrayDto: typing.Type[ArrayDto]
    ArrayDtoAsScalarArray: typing.Type[ArrayDtoAsScalarArray]
    BooleanArrayDto: typing.Type[BooleanArrayDto]
    ByteArrayDto: typing.Type[ByteArrayDto]
    DoubleArrayDto: typing.Type[DoubleArrayDto]
    FloatArrayDto: typing.Type[FloatArrayDto]
    IntArrayDto: typing.Type[IntArrayDto]
    LongArrayDto: typing.Type[LongArrayDto]
    ShortArrayDto: typing.Type[ShortArrayDto]
    StringArrayDto: typing.Type[StringArrayDto]
    ToArrayDto: typing.Type[ToArrayDto]
