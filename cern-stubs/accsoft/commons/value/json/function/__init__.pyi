
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.function.discrete
import cern.accsoft.commons.value.json.function.polynomial
import typing



_FunctionDto__D = typing.TypeVar('_FunctionDto__D', bound=cern.accsoft.commons.value.Function)  # <D>
class FunctionDto(cern.accsoft.commons.value.json.common.TypedDto[_FunctionDto__D], typing.Generic[_FunctionDto__D]):
    """
    public interface FunctionDto<D extends cern.accsoft.commons.value.Function> extends :class:`~cern.accsoft.commons.value.json.common.TypedDto`<D>
    """
    ...

_MathFunctionDto__D = typing.TypeVar('_MathFunctionDto__D', bound=cern.accsoft.commons.value.MathFunction)  # <D>
class MathFunctionDto(FunctionDto[_MathFunctionDto__D], typing.Generic[_MathFunctionDto__D]):
    """
    public interface MathFunctionDto<D extends cern.accsoft.commons.value.MathFunction> extends :class:`~cern.accsoft.commons.value.json.function.FunctionDto`<D>
    """
    ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.function")``.

    FunctionDto: typing.Type[FunctionDto]
    MathFunctionDto: typing.Type[MathFunctionDto]
    discrete: cern.accsoft.commons.value.json.function.discrete.__module_protocol__
    polynomial: cern.accsoft.commons.value.json.function.polynomial.__module_protocol__
