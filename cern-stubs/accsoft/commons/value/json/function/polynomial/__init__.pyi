
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.function
import cern.accsoft.commons.value.json.function.polynomial.bounded
import jpype
import typing



_PolynomialDto__D = typing.TypeVar('_PolynomialDto__D', bound=cern.accsoft.commons.value.Polynomial)  # <D>
_PolynomialDto__R = typing.TypeVar('_PolynomialDto__R')  # <R>
class PolynomialDto(cern.accsoft.commons.value.json.common.ValueDto[_PolynomialDto__D, _PolynomialDto__R], cern.accsoft.commons.value.json.function.MathFunctionDto[_PolynomialDto__D], typing.Generic[_PolynomialDto__D, _PolynomialDto__R]):
    """
    public abstract class PolynomialDto<D extends cern.accsoft.commons.value.Polynomial, R> extends :class:`~cern.accsoft.commons.value.json.common.ValueDto`<D, R> implements :class:`~cern.accsoft.commons.value.json.function.MathFunctionDto`<D>
    """
    ...

class ToPolynomialDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.Polynomial, 'PolynomialImplDto']):
    """
    public class ToPolynomialDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.Polynomial, :class:`~cern.accsoft.commons.value.json.function.polynomial.PolynomialImplDto`>
    """
    def __init__(self): ...
    def convert(self, polynomial: cern.accsoft.commons.value.Polynomial) -> 'PolynomialImplDto':
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class PolynomialImplDto(PolynomialDto[cern.accsoft.commons.value.Polynomial, typing.MutableSequence[float]]):
    """
    public class PolynomialImplDto extends :class:`~cern.accsoft.commons.value.json.function.polynomial.PolynomialDto`<cern.accsoft.commons.value.Polynomial, double[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, doubleArray: typing.Union[typing.List[float], jpype.JArray]): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.function.polynomial")``.

    PolynomialDto: typing.Type[PolynomialDto]
    PolynomialImplDto: typing.Type[PolynomialImplDto]
    ToPolynomialDto: typing.Type[ToPolynomialDto]
    bounded: cern.accsoft.commons.value.json.function.polynomial.bounded.__module_protocol__
