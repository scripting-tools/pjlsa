
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.function
import typing



_BoundedPolynomialSequenceDto__R = typing.TypeVar('_BoundedPolynomialSequenceDto__R')  # <R>
class BoundedPolynomialSequenceDto(cern.accsoft.commons.value.json.common.ValueDto[cern.accsoft.commons.value.BoundedPolynomialSequence, _BoundedPolynomialSequenceDto__R], cern.accsoft.commons.value.json.function.MathFunctionDto[cern.accsoft.commons.value.BoundedPolynomialSequence], typing.Generic[_BoundedPolynomialSequenceDto__R]):
    """
    public abstract class BoundedPolynomialSequenceDto<R> extends :class:`~cern.accsoft.commons.value.json.common.ValueDto`<cern.accsoft.commons.value.BoundedPolynomialSequence, R> implements :class:`~cern.accsoft.commons.value.json.function.MathFunctionDto`<cern.accsoft.commons.value.BoundedPolynomialSequence>
    """
    ...

class ToBoundedPolynomialSequenceDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.BoundedPolynomialSequence, 'BoundedPolynomialSequenceImplDto']):
    """
    public class ToBoundedPolynomialSequenceDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.BoundedPolynomialSequence, :class:`~cern.accsoft.commons.value.json.function.polynomial.bounded.sequence.BoundedPolynomialSequenceImplDto`>
    """
    def __init__(self): ...
    def convert(self, boundedPolynomialSequence: cern.accsoft.commons.value.BoundedPolynomialSequence) -> 'BoundedPolynomialSequenceImplDto':
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class BoundedPolynomialSequenceImplDto(BoundedPolynomialSequenceDto[typing.MutableSequence[cern.accsoft.commons.value.BoundedPolynomial]]):
    """
    public class BoundedPolynomialSequenceImplDto extends :class:`~cern.accsoft.commons.value.json.function.polynomial.bounded.sequence.BoundedPolynomialSequenceDto`<cern.accsoft.commons.value.BoundedPolynomial[]>
    """
    ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.function.polynomial.bounded.sequence")``.

    BoundedPolynomialSequenceDto: typing.Type[BoundedPolynomialSequenceDto]
    BoundedPolynomialSequenceImplDto: typing.Type[BoundedPolynomialSequenceImplDto]
    ToBoundedPolynomialSequenceDto: typing.Type[ToBoundedPolynomialSequenceDto]
