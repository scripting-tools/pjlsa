
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.value
import cern.accsoft.commons.value.json.common
import cern.accsoft.commons.value.json.function.discrete
import jpype
import typing



_DiscreteFunctionListDto__R = typing.TypeVar('_DiscreteFunctionListDto__R')  # <R>
class DiscreteFunctionListDto(cern.accsoft.commons.value.json.function.discrete.DiscreteFunctionDto[cern.accsoft.commons.value.DiscreteFunctionList, _DiscreteFunctionListDto__R], typing.Generic[_DiscreteFunctionListDto__R]):
    """
    public abstract class DiscreteFunctionListDto<R> extends :class:`~cern.accsoft.commons.value.json.function.discrete.DiscreteFunctionDto`<cern.accsoft.commons.value.DiscreteFunctionList, R>
    """
    ...

class ToDiscreteFunctionListDto(cern.accsoft.commons.value.json.common.TypedToDtoConverter[cern.accsoft.commons.value.ImmutableDiscreteFunctionList, 'DiscreteFunctionListImplDto']):
    """
    public class ToDiscreteFunctionListDto extends java.lang.Object implements :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`<cern.accsoft.commons.value.ImmutableDiscreteFunctionList, :class:`~cern.accsoft.commons.value.json.function.discrete.list.DiscreteFunctionListImplDto`>
    """
    def __init__(self): ...
    def convert(self, immutableDiscreteFunctionList: cern.accsoft.commons.value.ImmutableDiscreteFunctionList) -> 'DiscreteFunctionListImplDto':
        """
        
            Specified by:
                :meth:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter.convert`Â in
                interfaceÂ :class:`~cern.accsoft.commons.value.json.common.TypedToDtoConverter`
        
        
        """
        ...

class DiscreteFunctionListImplDto(DiscreteFunctionListDto[typing.MutableSequence[cern.accsoft.commons.value.ImmutableDiscreteFunction]]):
    """
    public class DiscreteFunctionListImplDto extends :class:`~cern.accsoft.commons.value.json.function.discrete.list.DiscreteFunctionListDto`<cern.accsoft.commons.value.ImmutableDiscreteFunction[]>
    """
    def __init__(self, valueDescriptor: cern.accsoft.commons.value.ValueDescriptor, immutableDiscreteFunctionArray: typing.Union[typing.List[cern.accsoft.commons.value.ImmutableDiscreteFunction], jpype.JArray]): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.value.json.function.discrete.list")``.

    DiscreteFunctionListDto: typing.Type[DiscreteFunctionListDto]
    DiscreteFunctionListImplDto: typing.Type[DiscreteFunctionListImplDto]
    ToDiscreteFunctionListDto: typing.Type[ToDiscreteFunctionListDto]
