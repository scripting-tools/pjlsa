
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.io
import typing



class RbaReflectiveInvoker:
    def __init__(self): ...
    def clearMiddleTierRbaToken(self) -> None: ...
    def findRbaToken(self) -> java.io.Serializable: ...
    def getHostName(self) -> str: ...
    def getUserName(self) -> str: ...
    def setMiddleTierRbaToken(self, serializable: java.io.Serializable) -> None: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.rba")``.

    RbaReflectiveInvoker: typing.Type[RbaReflectiveInvoker]
