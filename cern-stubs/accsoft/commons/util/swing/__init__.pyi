
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.awt
import javax.swing
import javax.swing.plaf
import javax.swing.plaf.metal
import typing



class WorkaroundMetalToolTipUI(javax.swing.plaf.metal.MetalToolTipUI):
    def __init__(self): ...
    @staticmethod
    def createUI(jComponent: javax.swing.JComponent) -> javax.swing.plaf.ComponentUI: ...
    @staticmethod
    def installWorkaround() -> None: ...
    def paint(self, graphics: java.awt.Graphics, jComponent: javax.swing.JComponent) -> None: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.swing")``.

    WorkaroundMetalToolTipUI: typing.Type[WorkaroundMetalToolTipUI]
