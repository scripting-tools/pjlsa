
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.lang
import java.util
import java.util.concurrent
import typing



class DefaultThreadFactory(java.util.concurrent.ThreadFactory):
    @typing.overload
    def __init__(self, string: str): ...
    @typing.overload
    def __init__(self, string: str, boolean: bool): ...
    def newThread(self, runnable: typing.Union[java.lang.Runnable, typing.Callable]) -> java.lang.Thread: ...

_ResourceSynchronizationHelper__Command__R = typing.TypeVar('_ResourceSynchronizationHelper__Command__R')  # <R>
_ResourceSynchronizationHelper__T = typing.TypeVar('_ResourceSynchronizationHelper__T')  # <T>
class ResourceSynchronizationHelper(typing.Generic[_ResourceSynchronizationHelper__T]):
    @typing.overload
    def __init__(self): ...
    @typing.overload
    def __init__(self, comparator: typing.Union[java.util.Comparator[_ResourceSynchronizationHelper__T], typing.Callable[[_ResourceSynchronizationHelper__T, _ResourceSynchronizationHelper__T], int]]): ...
    _executeCommand__R = typing.TypeVar('_executeCommand__R')  # <R>
    def executeCommand(self, collection: typing.Union[java.util.Collection[_ResourceSynchronizationHelper__T], typing.Sequence[_ResourceSynchronizationHelper__T], typing.Set[_ResourceSynchronizationHelper__T]], command: 'ResourceSynchronizationHelper.Command'[_executeCommand__R]) -> _executeCommand__R: ...
    class Command(typing.Generic[_ResourceSynchronizationHelper__Command__R]):
        def execute(self) -> _ResourceSynchronizationHelper__Command__R: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.concurrent")``.

    DefaultThreadFactory: typing.Type[DefaultThreadFactory]
    ResourceSynchronizationHelper: typing.Type[ResourceSynchronizationHelper]
