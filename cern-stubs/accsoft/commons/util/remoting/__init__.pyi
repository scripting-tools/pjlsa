
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.accsoft.commons.util.remoting.instrumentation
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.remoting")``.

    instrumentation: cern.accsoft.commons.util.remoting.instrumentation.__module_protocol__
