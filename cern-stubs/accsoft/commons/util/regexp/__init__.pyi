
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import typing



class RegexpUtils:
    def __init__(self): ...
    @staticmethod
    def escapeSpecialCharacters(string: str) -> str: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.regexp")``.

    RegexpUtils: typing.Type[RegexpUtils]
