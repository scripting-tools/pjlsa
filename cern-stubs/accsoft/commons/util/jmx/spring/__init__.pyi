
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import javax.management
import org.springframework.jmx.export
import typing



class ApplicationMBeanExporter(org.springframework.jmx.export.MBeanExporter):
    def __init__(self): ...
    def setServer(self, mBeanServer: javax.management.MBeanServer) -> None: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.accsoft.commons.util.jmx.spring")``.

    ApplicationMBeanExporter: typing.Type[ApplicationMBeanExporter]
