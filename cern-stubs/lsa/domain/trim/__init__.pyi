
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.domain.trim.rules
import cern.lsa.domain.trim.tag
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.trim")``.

    rules: cern.lsa.domain.trim.rules.__module_protocol__
    tag: cern.lsa.domain.trim.tag.__module_protocol__
