
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import com.fasterxml.jackson.databind.util
import datetime
import java.io
import java.time
import java.util
import typing



class DeconstructedMap:
    @typing.overload
    def __init__(self): ...
    @typing.overload
    def __init__(self, map: typing.Union[java.util.Map[str, typing.Any], typing.Mapping[str, typing.Any]]): ...
    def getInstantAttributes(self) -> java.util.Map[str, java.time.Instant]: ...
    def getPrimitiveAttributes(self) -> java.util.Map[str, typing.Any]: ...
    def getSerializableRequestAttributes(self) -> java.util.Map[str, 'SerializableRequestAttribute']: ...
    def reconstructMap(self) -> java.util.Map[str, typing.Any]: ...
    def setInstantAttributes(self, map: typing.Union[java.util.Map[str, typing.Union[java.time.Instant, datetime.datetime]], typing.Mapping[str, typing.Union[java.time.Instant, datetime.datetime]]]) -> None: ...
    def setPrimitiveAttributes(self, map: typing.Union[java.util.Map[str, typing.Any], typing.Mapping[str, typing.Any]]) -> None: ...
    def setSerializableRequestAttributes(self, map: typing.Union[java.util.Map[str, 'SerializableRequestAttribute'], typing.Mapping[str, 'SerializableRequestAttribute']]) -> None: ...

class DeconstructedMapToObjectMapConverter(com.fasterxml.jackson.databind.util.StdConverter[DeconstructedMap, java.util.Map[str, typing.Any]]):
    def __init__(self): ...
    def convert(self, deconstructedMap: DeconstructedMap) -> java.util.Map[str, typing.Any]: ...

class ObjectMapToDeconstructedMapConverter(com.fasterxml.jackson.databind.util.StdConverter[java.util.Map[str, typing.Any], DeconstructedMap]):
    def __init__(self): ...
    def convert(self, map: typing.Union[java.util.Map[str, typing.Any], typing.Mapping[str, typing.Any]]) -> DeconstructedMap: ...

class SerializableRequestAttribute(java.io.Serializable): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.commons.json")``.

    DeconstructedMap: typing.Type[DeconstructedMap]
    DeconstructedMapToObjectMapConverter: typing.Type[DeconstructedMapToObjectMapConverter]
    ObjectMapToDeconstructedMapConverter: typing.Type[ObjectMapToDeconstructedMapConverter]
    SerializableRequestAttribute: typing.Type[SerializableRequestAttribute]
