
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.lang
import java.lang.reflect
import typing



class CacheUtil:
    @staticmethod
    def collectionToCacheKey(iterable: java.lang.Iterable[typing.Any]) -> str: ...
    @staticmethod
    def generateCacheKey(method: java.lang.reflect.Method, *object: typing.Any) -> str: ...

class CustomCacheKeyProvider:
    def getCacheKey(self) -> str: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.commons.cache")``.

    CacheUtil: typing.Type[CacheUtil]
    CustomCacheKeyProvider: typing.Type[CustomCacheKeyProvider]
