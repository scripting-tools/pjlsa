
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import java.lang
import typing



class ParameterExceptionCode(java.lang.Enum['ParameterExceptionCode']):
    USER_IS_NOT_MAPPED: typing.ClassVar['ParameterExceptionCode'] = ...
    USER_WAS_UNMAPPED_FROM_ALL_CONTEXTS: typing.ClassVar['ParameterExceptionCode'] = ...
    NO_SETTINGS_IN_NEW_CONTEXT: typing.ClassVar['ParameterExceptionCode'] = ...
    NO_SETTINGS: typing.ClassVar['ParameterExceptionCode'] = ...
    def getCode(self) -> int: ...
    _valueOf_1__T = typing.TypeVar('_valueOf_1__T', bound=java.lang.Enum)  # <T>
    @typing.overload
    @staticmethod
    def valueOf(string: str) -> 'ParameterExceptionCode': ...
    @typing.overload
    @staticmethod
    def valueOf(class_: typing.Type[_valueOf_1__T], string: str) -> _valueOf_1__T: ...
    @staticmethod
    def values() -> typing.MutableSequence['ParameterExceptionCode']: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.commons.japc")``.

    ParameterExceptionCode: typing.Type[ParameterExceptionCode]
