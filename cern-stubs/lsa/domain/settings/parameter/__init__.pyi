
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.domain.settings.parameter.relation
import cern.lsa.domain.settings.parameter.type
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.settings.parameter")``.

    relation: cern.lsa.domain.settings.parameter.relation.__module_protocol__
    type: cern.lsa.domain.settings.parameter.type.__module_protocol__
