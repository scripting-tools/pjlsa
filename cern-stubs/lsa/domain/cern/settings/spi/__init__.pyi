
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.domain.cern.settings.spi.ad
import cern.lsa.domain.cern.settings.spi.elena
import cern.lsa.domain.cern.settings.spi.lktim
import typing


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.domain.cern.settings.spi")``.

    ad: cern.lsa.domain.cern.settings.spi.ad.__module_protocol__
    elena: cern.lsa.domain.cern.settings.spi.elena.__module_protocol__
    lktim: cern.lsa.domain.cern.settings.spi.lktim.__module_protocol__
