
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.domain.cern.optics.ofb
import cern.lsa.domain.cern.settings.lktim
import cern.lsa.domain.settings
import java.io
import java.util
import typing



class CalculateKFromIrefRequest:
    @staticmethod
    def builder() -> 'DefaultCalculateKFromIrefRequest.Builder': ...
    def getEnergy(self) -> float: ...
    def getPowerConverterCurrentMap(self) -> java.util.Map[str, float]: ...

class CheckDeviceForAdditionIntoLktimTreeRequest:
    @staticmethod
    def builder() -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest.Builder': ...
    def getDeviceName(self) -> str: ...
    def getLktimTree(self) -> cern.lsa.domain.cern.settings.lktim.LktimTree: ...

class GroupBctMarkersByTimingUserRequest:
    @staticmethod
    def byTimingUsers(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'GroupBctMarkersByTimingUserRequest': ...
    def check(self) -> None: ...
    @staticmethod
    def forActiveContexts() -> 'GroupBctMarkersByTimingUserRequest': ...
    @staticmethod
    def forResidentContexts() -> 'GroupBctMarkersByTimingUserRequest': ...
    def getTimingUsers(self) -> java.util.Collection[str]: ...
    def isForActiveContexts(self) -> bool: ...
    def isForResidentContexts(self) -> bool: ...

class GroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest:
    @staticmethod
    def builder() -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest.Builder': ...
    def getBeamModeCategory(self) -> str: ...
    def getCollimatorNames(self) -> java.util.Collection[str]: ...
    def getValidFrom(self) -> java.util.Date: ...
    def getValidTo(self) -> java.util.Date: ...

class GroupNamesByDeviceNameRequest:
    @staticmethod
    def builder() -> 'DefaultGroupNamesByDeviceNameRequest.Builder': ...
    def getDeviceNames(self) -> java.util.Collection[str]: ...
    def getTime(self) -> java.util.Date: ...

class GroupResidentStandAloneBeamProcessesByUserGroupRequest:
    @staticmethod
    def builder() -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest.Builder': ...
    def getUtcTimestampFrom(self) -> int: ...
    def getUtcTimestampTo(self) -> int: ...

class LhcFillDirectoryCreationRequest:
    @staticmethod
    def builder() -> 'DefaultLhcFillDirectoryCreationRequest.Builder': ...
    @staticmethod
    def byFillNumber(int: int) -> 'LhcFillDirectoryCreationRequest': ...
    def getFillNumber(self) -> int: ...

class LhcOfbReadingSetsRequest:
    @staticmethod
    def builder() -> 'DefaultLhcOfbReadingSetsRequest.Builder': ...
    @staticmethod
    def byOfbReadingSetHeaders(collection: typing.Union[java.util.Collection[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader], typing.Sequence[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader]]) -> 'LhcOfbReadingSetsRequest': ...
    def getOfbReadingSetHeaders(self) -> java.util.Collection[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader]: ...

class LhcSetHandshakePropertyRequest:
    @staticmethod
    def builder() -> 'DefaultLhcSetHandshakePropertyRequest.Builder': ...
    def getPropertyName(self) -> str: ...
    def getPropertyValue(self) -> str: ...

class LktimTreesRequest:
    @staticmethod
    def builder() -> 'DefaultLktimTreesRequest.Builder': ...
    def getName(self) -> str: ...
    def getTime(self) -> java.util.Date: ...

class MapLhcBlmInfoByExpertNameRequest:
    @staticmethod
    def builder() -> 'DefaultMapLhcBlmInfoByExpertNameRequest.Builder': ...
    @staticmethod
    def byCrateName(string: str) -> 'MapLhcBlmInfoByExpertNameRequest': ...
    def getCrateName(self) -> str: ...

class MapLhcCollimatorAlignmentByCollimatorNameRequest:
    @staticmethod
    def builder() -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest.Builder': ...
    def getBeamModeCategory(self) -> str: ...
    def getCollimatorNames(self) -> java.util.Collection[str]: ...

class MapLhcCollimatorInfoByNameRequest:
    @staticmethod
    def builder() -> 'DefaultMapLhcCollimatorInfoByNameRequest.Builder': ...
    @staticmethod
    def byDeviceNames(collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'MapLhcCollimatorInfoByNameRequest': ...
    def getDeviceNames(self) -> java.util.Collection[str]: ...

class MapResidentStandAloneBeamProcessByUserGroupRequest:
    @staticmethod
    def builder() -> 'DefaultMapResidentStandAloneBeamProcessByUserGroupRequest.Builder': ...
    @staticmethod
    def byUtcTimestamp(long: int) -> 'MapResidentStandAloneBeamProcessByUserGroupRequest': ...
    def getUtcTimestamp(self) -> int: ...

class MapStatusByTreeNameRequest:
    @staticmethod
    def builder() -> 'DefaultMapStatusByTreeNameRequest.Builder': ...
    def getStandAloneCycle(self) -> cern.lsa.domain.settings.StandAloneCycle: ...
    def getTreeNames(self) -> java.util.Collection[str]: ...

class SaveLktimTreeSettingsRequest:
    @staticmethod
    def builder() -> 'DefaultSaveLktimTreeSettingsRequest.Builder': ...
    def getLktimTree(self) -> cern.lsa.domain.cern.settings.lktim.LktimTree: ...
    def getStandAloneCycle(self) -> cern.lsa.domain.settings.StandAloneCycle: ...

class SearchBctMarkersRequest:
    @staticmethod
    def builder() -> 'DefaultSearchBctMarkersRequest.Builder': ...
    @staticmethod
    def byStandAloneContext(standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> 'SearchBctMarkersRequest': ...
    def getStandAloneContext(self) -> cern.lsa.domain.settings.StandAloneContext: ...

class SearchLktimTreeSettingsRequest:
    @staticmethod
    def builder() -> 'DefaultSearchLktimTreeSettingsRequest.Builder': ...
    def getLktimTree(self) -> cern.lsa.domain.cern.settings.lktim.LktimTree: ...
    def getStandAloneCycle(self) -> cern.lsa.domain.settings.StandAloneCycle: ...

class DefaultCalculateKFromIrefRequest(CalculateKFromIrefRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultCalculateKFromIrefRequest.Builder': ...
    @staticmethod
    def copyOf(calculateKFromIrefRequest: CalculateKFromIrefRequest) -> 'DefaultCalculateKFromIrefRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getEnergy(self) -> float: ...
    def getPowerConverterCurrentMap(self) -> java.util.Map[str, float]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withEnergy(self, double: float) -> 'DefaultCalculateKFromIrefRequest': ...
    def withPowerConverterCurrentMap(self, map: typing.Union[java.util.Map[str, float], typing.Mapping[str, float]]) -> 'DefaultCalculateKFromIrefRequest': ...
    class Builder:
        def build(self) -> 'DefaultCalculateKFromIrefRequest': ...
        def energy(self, double: float) -> 'DefaultCalculateKFromIrefRequest.Builder': ...
        def from_(self, calculateKFromIrefRequest: CalculateKFromIrefRequest) -> 'DefaultCalculateKFromIrefRequest.Builder': ...
        def powerConverterCurrentMap(self, map: typing.Union[java.util.Map[str, float], typing.Mapping[str, float]]) -> 'DefaultCalculateKFromIrefRequest.Builder': ...
        def putAllPowerConverterCurrentMap(self, map: typing.Union[java.util.Map[str, float], typing.Mapping[str, float]]) -> 'DefaultCalculateKFromIrefRequest.Builder': ...
        @typing.overload
        def putPowerConverterCurrentMap(self, string: str, double: float) -> 'DefaultCalculateKFromIrefRequest.Builder': ...
        @typing.overload
        def putPowerConverterCurrentMap(self, entry: java.util.Map.Entry[str, float]) -> 'DefaultCalculateKFromIrefRequest.Builder': ...

class DefaultCheckDeviceForAdditionIntoLktimTreeRequest(CheckDeviceForAdditionIntoLktimTreeRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest.Builder': ...
    @staticmethod
    def copyOf(checkDeviceForAdditionIntoLktimTreeRequest: CheckDeviceForAdditionIntoLktimTreeRequest) -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getDeviceName(self) -> str: ...
    def getLktimTree(self) -> cern.lsa.domain.cern.settings.lktim.LktimTree: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withDeviceName(self, string: str) -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest': ...
    def withLktimTree(self, lktimTree: cern.lsa.domain.cern.settings.lktim.LktimTree) -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest': ...
    class Builder:
        def build(self) -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest': ...
        def deviceName(self, string: str) -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest.Builder': ...
        def from_(self, checkDeviceForAdditionIntoLktimTreeRequest: CheckDeviceForAdditionIntoLktimTreeRequest) -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest.Builder': ...
        def lktimTree(self, lktimTree: cern.lsa.domain.cern.settings.lktim.LktimTree) -> 'DefaultCheckDeviceForAdditionIntoLktimTreeRequest.Builder': ...

class DefaultGroupBctMarkersByTimingUserRequest(GroupBctMarkersByTimingUserRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultGroupBctMarkersByTimingUserRequest.Builder': ...
    @staticmethod
    def copyOf(groupBctMarkersByTimingUserRequest: GroupBctMarkersByTimingUserRequest) -> 'DefaultGroupBctMarkersByTimingUserRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getTimingUsers(self) -> java.util.Collection[str]: ...
    def hashCode(self) -> int: ...
    def isForActiveContexts(self) -> bool: ...
    def isForResidentContexts(self) -> bool: ...
    def toString(self) -> str: ...
    def withForActiveContexts(self, boolean: bool) -> 'DefaultGroupBctMarkersByTimingUserRequest': ...
    def withForResidentContexts(self, boolean: bool) -> 'DefaultGroupBctMarkersByTimingUserRequest': ...
    def withTimingUsers(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultGroupBctMarkersByTimingUserRequest': ...
    class Builder:
        def build(self) -> 'DefaultGroupBctMarkersByTimingUserRequest': ...
        def forActiveContexts(self, boolean: bool) -> 'DefaultGroupBctMarkersByTimingUserRequest.Builder': ...
        def forResidentContexts(self, boolean: bool) -> 'DefaultGroupBctMarkersByTimingUserRequest.Builder': ...
        def from_(self, groupBctMarkersByTimingUserRequest: GroupBctMarkersByTimingUserRequest) -> 'DefaultGroupBctMarkersByTimingUserRequest.Builder': ...
        def timingUsers(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultGroupBctMarkersByTimingUserRequest.Builder': ...

class DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest(GroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest.Builder': ...
    @staticmethod
    def copyOf(groupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest: GroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getBeamModeCategory(self) -> str: ...
    def getCollimatorNames(self) -> java.util.Collection[str]: ...
    def getValidFrom(self) -> java.util.Date: ...
    def getValidTo(self) -> java.util.Date: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withBeamModeCategory(self, string: str) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest': ...
    def withCollimatorNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest': ...
    def withValidFrom(self, date: java.util.Date) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest': ...
    def withValidTo(self, date: java.util.Date) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest': ...
    class Builder:
        def beamModeCategory(self, string: str) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest.Builder': ...
        def build(self) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest': ...
        def collimatorNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest.Builder': ...
        def from_(self, groupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest: GroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest.Builder': ...
        def validFrom(self, date: java.util.Date) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest.Builder': ...
        def validTo(self, date: java.util.Date) -> 'DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest.Builder': ...

class DefaultGroupNamesByDeviceNameRequest(GroupNamesByDeviceNameRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultGroupNamesByDeviceNameRequest.Builder': ...
    @staticmethod
    def copyOf(groupNamesByDeviceNameRequest: GroupNamesByDeviceNameRequest) -> 'DefaultGroupNamesByDeviceNameRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getDeviceNames(self) -> java.util.Collection[str]: ...
    def getTime(self) -> java.util.Date: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withDeviceNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultGroupNamesByDeviceNameRequest': ...
    def withTime(self, date: java.util.Date) -> 'DefaultGroupNamesByDeviceNameRequest': ...
    class Builder:
        def build(self) -> 'DefaultGroupNamesByDeviceNameRequest': ...
        def deviceNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultGroupNamesByDeviceNameRequest.Builder': ...
        def from_(self, groupNamesByDeviceNameRequest: GroupNamesByDeviceNameRequest) -> 'DefaultGroupNamesByDeviceNameRequest.Builder': ...
        def time(self, date: java.util.Date) -> 'DefaultGroupNamesByDeviceNameRequest.Builder': ...

class DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest(GroupResidentStandAloneBeamProcessesByUserGroupRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest.Builder': ...
    @staticmethod
    def copyOf(groupResidentStandAloneBeamProcessesByUserGroupRequest: GroupResidentStandAloneBeamProcessesByUserGroupRequest) -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getUtcTimestampFrom(self) -> int: ...
    def getUtcTimestampTo(self) -> int: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withUtcTimestampFrom(self, long: int) -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest': ...
    def withUtcTimestampTo(self, long: int) -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest': ...
    class Builder:
        def build(self) -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest': ...
        def from_(self, groupResidentStandAloneBeamProcessesByUserGroupRequest: GroupResidentStandAloneBeamProcessesByUserGroupRequest) -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest.Builder': ...
        def utcTimestampFrom(self, long: int) -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest.Builder': ...
        def utcTimestampTo(self, long: int) -> 'DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest.Builder': ...

class DefaultLhcFillDirectoryCreationRequest(LhcFillDirectoryCreationRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultLhcFillDirectoryCreationRequest.Builder': ...
    @staticmethod
    def copyOf(lhcFillDirectoryCreationRequest: LhcFillDirectoryCreationRequest) -> 'DefaultLhcFillDirectoryCreationRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getFillNumber(self) -> int: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withFillNumber(self, int: int) -> 'DefaultLhcFillDirectoryCreationRequest': ...
    class Builder:
        def build(self) -> 'DefaultLhcFillDirectoryCreationRequest': ...
        def fillNumber(self, int: int) -> 'DefaultLhcFillDirectoryCreationRequest.Builder': ...
        def from_(self, lhcFillDirectoryCreationRequest: LhcFillDirectoryCreationRequest) -> 'DefaultLhcFillDirectoryCreationRequest.Builder': ...

class DefaultLhcOfbReadingSetsRequest(LhcOfbReadingSetsRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultLhcOfbReadingSetsRequest.Builder': ...
    @staticmethod
    def copyOf(lhcOfbReadingSetsRequest: LhcOfbReadingSetsRequest) -> 'DefaultLhcOfbReadingSetsRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getOfbReadingSetHeaders(self) -> java.util.Collection[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withOfbReadingSetHeaders(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader], typing.Sequence[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader]]) -> 'DefaultLhcOfbReadingSetsRequest': ...
    class Builder:
        def build(self) -> 'DefaultLhcOfbReadingSetsRequest': ...
        def from_(self, lhcOfbReadingSetsRequest: LhcOfbReadingSetsRequest) -> 'DefaultLhcOfbReadingSetsRequest.Builder': ...
        def ofbReadingSetHeaders(self, collection: typing.Union[java.util.Collection[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader], typing.Sequence[cern.lsa.domain.cern.optics.ofb.OfbReadingSetHeader]]) -> 'DefaultLhcOfbReadingSetsRequest.Builder': ...

class DefaultLhcSetHandshakePropertyRequest(LhcSetHandshakePropertyRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultLhcSetHandshakePropertyRequest.Builder': ...
    @staticmethod
    def copyOf(lhcSetHandshakePropertyRequest: LhcSetHandshakePropertyRequest) -> 'DefaultLhcSetHandshakePropertyRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getPropertyName(self) -> str: ...
    def getPropertyValue(self) -> str: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withPropertyName(self, string: str) -> 'DefaultLhcSetHandshakePropertyRequest': ...
    def withPropertyValue(self, string: str) -> 'DefaultLhcSetHandshakePropertyRequest': ...
    class Builder:
        def build(self) -> 'DefaultLhcSetHandshakePropertyRequest': ...
        def from_(self, lhcSetHandshakePropertyRequest: LhcSetHandshakePropertyRequest) -> 'DefaultLhcSetHandshakePropertyRequest.Builder': ...
        def propertyName(self, string: str) -> 'DefaultLhcSetHandshakePropertyRequest.Builder': ...
        def propertyValue(self, string: str) -> 'DefaultLhcSetHandshakePropertyRequest.Builder': ...

class DefaultLktimTreesRequest(LktimTreesRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultLktimTreesRequest.Builder': ...
    @staticmethod
    def copyOf(lktimTreesRequest: LktimTreesRequest) -> 'DefaultLktimTreesRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getName(self) -> str: ...
    def getTime(self) -> java.util.Date: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withName(self, string: str) -> 'DefaultLktimTreesRequest': ...
    def withTime(self, date: java.util.Date) -> 'DefaultLktimTreesRequest': ...
    class Builder:
        def build(self) -> 'DefaultLktimTreesRequest': ...
        def from_(self, lktimTreesRequest: LktimTreesRequest) -> 'DefaultLktimTreesRequest.Builder': ...
        def name(self, string: str) -> 'DefaultLktimTreesRequest.Builder': ...
        def time(self, date: java.util.Date) -> 'DefaultLktimTreesRequest.Builder': ...

class DefaultMapLhcBlmInfoByExpertNameRequest(MapLhcBlmInfoByExpertNameRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultMapLhcBlmInfoByExpertNameRequest.Builder': ...
    @staticmethod
    def copyOf(mapLhcBlmInfoByExpertNameRequest: MapLhcBlmInfoByExpertNameRequest) -> 'DefaultMapLhcBlmInfoByExpertNameRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getCrateName(self) -> str: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withCrateName(self, string: str) -> 'DefaultMapLhcBlmInfoByExpertNameRequest': ...
    class Builder:
        def build(self) -> 'DefaultMapLhcBlmInfoByExpertNameRequest': ...
        def crateName(self, string: str) -> 'DefaultMapLhcBlmInfoByExpertNameRequest.Builder': ...
        def from_(self, mapLhcBlmInfoByExpertNameRequest: MapLhcBlmInfoByExpertNameRequest) -> 'DefaultMapLhcBlmInfoByExpertNameRequest.Builder': ...

class DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest(MapLhcCollimatorAlignmentByCollimatorNameRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest.Builder': ...
    @staticmethod
    def copyOf(mapLhcCollimatorAlignmentByCollimatorNameRequest: MapLhcCollimatorAlignmentByCollimatorNameRequest) -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getBeamModeCategory(self) -> str: ...
    def getCollimatorNames(self) -> java.util.Collection[str]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withBeamModeCategory(self, string: str) -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest': ...
    def withCollimatorNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest': ...
    class Builder:
        def beamModeCategory(self, string: str) -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest.Builder': ...
        def build(self) -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest': ...
        def collimatorNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest.Builder': ...
        def from_(self, mapLhcCollimatorAlignmentByCollimatorNameRequest: MapLhcCollimatorAlignmentByCollimatorNameRequest) -> 'DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest.Builder': ...

class DefaultMapLhcCollimatorInfoByNameRequest(MapLhcCollimatorInfoByNameRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultMapLhcCollimatorInfoByNameRequest.Builder': ...
    @staticmethod
    def copyOf(mapLhcCollimatorInfoByNameRequest: MapLhcCollimatorInfoByNameRequest) -> 'DefaultMapLhcCollimatorInfoByNameRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getDeviceNames(self) -> java.util.Collection[str]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withDeviceNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultMapLhcCollimatorInfoByNameRequest': ...
    class Builder:
        def build(self) -> 'DefaultMapLhcCollimatorInfoByNameRequest': ...
        def deviceNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultMapLhcCollimatorInfoByNameRequest.Builder': ...
        def from_(self, mapLhcCollimatorInfoByNameRequest: MapLhcCollimatorInfoByNameRequest) -> 'DefaultMapLhcCollimatorInfoByNameRequest.Builder': ...

class DefaultMapResidentStandAloneBeamProcessByUserGroupRequest(MapResidentStandAloneBeamProcessByUserGroupRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultMapResidentStandAloneBeamProcessByUserGroupRequest.Builder': ...
    @staticmethod
    def copyOf(mapResidentStandAloneBeamProcessByUserGroupRequest: MapResidentStandAloneBeamProcessByUserGroupRequest) -> 'DefaultMapResidentStandAloneBeamProcessByUserGroupRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getUtcTimestamp(self) -> int: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withUtcTimestamp(self, long: int) -> 'DefaultMapResidentStandAloneBeamProcessByUserGroupRequest': ...
    class Builder:
        def build(self) -> 'DefaultMapResidentStandAloneBeamProcessByUserGroupRequest': ...
        def from_(self, mapResidentStandAloneBeamProcessByUserGroupRequest: MapResidentStandAloneBeamProcessByUserGroupRequest) -> 'DefaultMapResidentStandAloneBeamProcessByUserGroupRequest.Builder': ...
        def utcTimestamp(self, long: int) -> 'DefaultMapResidentStandAloneBeamProcessByUserGroupRequest.Builder': ...

class DefaultMapStatusByTreeNameRequest(MapStatusByTreeNameRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultMapStatusByTreeNameRequest.Builder': ...
    @staticmethod
    def copyOf(mapStatusByTreeNameRequest: MapStatusByTreeNameRequest) -> 'DefaultMapStatusByTreeNameRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getStandAloneCycle(self) -> cern.lsa.domain.settings.StandAloneCycle: ...
    def getTreeNames(self) -> java.util.Collection[str]: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withStandAloneCycle(self, standAloneCycle: cern.lsa.domain.settings.StandAloneCycle) -> 'DefaultMapStatusByTreeNameRequest': ...
    def withTreeNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultMapStatusByTreeNameRequest': ...
    class Builder:
        def build(self) -> 'DefaultMapStatusByTreeNameRequest': ...
        def from_(self, mapStatusByTreeNameRequest: MapStatusByTreeNameRequest) -> 'DefaultMapStatusByTreeNameRequest.Builder': ...
        def standAloneCycle(self, standAloneCycle: cern.lsa.domain.settings.StandAloneCycle) -> 'DefaultMapStatusByTreeNameRequest.Builder': ...
        def treeNames(self, collection: typing.Union[java.util.Collection[str], typing.Sequence[str]]) -> 'DefaultMapStatusByTreeNameRequest.Builder': ...

class DefaultSaveLktimTreeSettingsRequest(SaveLktimTreeSettingsRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultSaveLktimTreeSettingsRequest.Builder': ...
    @staticmethod
    def copyOf(saveLktimTreeSettingsRequest: SaveLktimTreeSettingsRequest) -> 'DefaultSaveLktimTreeSettingsRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getLktimTree(self) -> cern.lsa.domain.cern.settings.lktim.LktimTree: ...
    def getStandAloneCycle(self) -> cern.lsa.domain.settings.StandAloneCycle: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withLktimTree(self, lktimTree: cern.lsa.domain.cern.settings.lktim.LktimTree) -> 'DefaultSaveLktimTreeSettingsRequest': ...
    def withStandAloneCycle(self, standAloneCycle: cern.lsa.domain.settings.StandAloneCycle) -> 'DefaultSaveLktimTreeSettingsRequest': ...
    class Builder:
        def build(self) -> 'DefaultSaveLktimTreeSettingsRequest': ...
        def from_(self, saveLktimTreeSettingsRequest: SaveLktimTreeSettingsRequest) -> 'DefaultSaveLktimTreeSettingsRequest.Builder': ...
        def lktimTree(self, lktimTree: cern.lsa.domain.cern.settings.lktim.LktimTree) -> 'DefaultSaveLktimTreeSettingsRequest.Builder': ...
        def standAloneCycle(self, standAloneCycle: cern.lsa.domain.settings.StandAloneCycle) -> 'DefaultSaveLktimTreeSettingsRequest.Builder': ...

class DefaultSearchBctMarkersRequest(SearchBctMarkersRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultSearchBctMarkersRequest.Builder': ...
    @staticmethod
    def copyOf(searchBctMarkersRequest: SearchBctMarkersRequest) -> 'DefaultSearchBctMarkersRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getStandAloneContext(self) -> cern.lsa.domain.settings.StandAloneContext: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withStandAloneContext(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> 'DefaultSearchBctMarkersRequest': ...
    class Builder:
        def build(self) -> 'DefaultSearchBctMarkersRequest': ...
        def from_(self, searchBctMarkersRequest: SearchBctMarkersRequest) -> 'DefaultSearchBctMarkersRequest.Builder': ...
        def standAloneContext(self, standAloneContext: cern.lsa.domain.settings.StandAloneContext) -> 'DefaultSearchBctMarkersRequest.Builder': ...

class DefaultSearchLktimTreeSettingsRequest(SearchLktimTreeSettingsRequest, java.io.Serializable):
    @staticmethod
    def builder() -> 'DefaultSearchLktimTreeSettingsRequest.Builder': ...
    @staticmethod
    def copyOf(searchLktimTreeSettingsRequest: SearchLktimTreeSettingsRequest) -> 'DefaultSearchLktimTreeSettingsRequest': ...
    def equals(self, object: typing.Any) -> bool: ...
    def getLktimTree(self) -> cern.lsa.domain.cern.settings.lktim.LktimTree: ...
    def getStandAloneCycle(self) -> cern.lsa.domain.settings.StandAloneCycle: ...
    def hashCode(self) -> int: ...
    def toString(self) -> str: ...
    def withLktimTree(self, lktimTree: cern.lsa.domain.cern.settings.lktim.LktimTree) -> 'DefaultSearchLktimTreeSettingsRequest': ...
    def withStandAloneCycle(self, standAloneCycle: cern.lsa.domain.settings.StandAloneCycle) -> 'DefaultSearchLktimTreeSettingsRequest': ...
    class Builder:
        def build(self) -> 'DefaultSearchLktimTreeSettingsRequest': ...
        def from_(self, searchLktimTreeSettingsRequest: SearchLktimTreeSettingsRequest) -> 'DefaultSearchLktimTreeSettingsRequest.Builder': ...
        def lktimTree(self, lktimTree: cern.lsa.domain.cern.settings.lktim.LktimTree) -> 'DefaultSearchLktimTreeSettingsRequest.Builder': ...
        def standAloneCycle(self, standAloneCycle: cern.lsa.domain.settings.StandAloneCycle) -> 'DefaultSearchLktimTreeSettingsRequest.Builder': ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.rest.cern.api.v1.dto")``.

    CalculateKFromIrefRequest: typing.Type[CalculateKFromIrefRequest]
    CheckDeviceForAdditionIntoLktimTreeRequest: typing.Type[CheckDeviceForAdditionIntoLktimTreeRequest]
    DefaultCalculateKFromIrefRequest: typing.Type[DefaultCalculateKFromIrefRequest]
    DefaultCheckDeviceForAdditionIntoLktimTreeRequest: typing.Type[DefaultCheckDeviceForAdditionIntoLktimTreeRequest]
    DefaultGroupBctMarkersByTimingUserRequest: typing.Type[DefaultGroupBctMarkersByTimingUserRequest]
    DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest: typing.Type[DefaultGroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest]
    DefaultGroupNamesByDeviceNameRequest: typing.Type[DefaultGroupNamesByDeviceNameRequest]
    DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest: typing.Type[DefaultGroupResidentStandAloneBeamProcessesByUserGroupRequest]
    DefaultLhcFillDirectoryCreationRequest: typing.Type[DefaultLhcFillDirectoryCreationRequest]
    DefaultLhcOfbReadingSetsRequest: typing.Type[DefaultLhcOfbReadingSetsRequest]
    DefaultLhcSetHandshakePropertyRequest: typing.Type[DefaultLhcSetHandshakePropertyRequest]
    DefaultLktimTreesRequest: typing.Type[DefaultLktimTreesRequest]
    DefaultMapLhcBlmInfoByExpertNameRequest: typing.Type[DefaultMapLhcBlmInfoByExpertNameRequest]
    DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest: typing.Type[DefaultMapLhcCollimatorAlignmentByCollimatorNameRequest]
    DefaultMapLhcCollimatorInfoByNameRequest: typing.Type[DefaultMapLhcCollimatorInfoByNameRequest]
    DefaultMapResidentStandAloneBeamProcessByUserGroupRequest: typing.Type[DefaultMapResidentStandAloneBeamProcessByUserGroupRequest]
    DefaultMapStatusByTreeNameRequest: typing.Type[DefaultMapStatusByTreeNameRequest]
    DefaultSaveLktimTreeSettingsRequest: typing.Type[DefaultSaveLktimTreeSettingsRequest]
    DefaultSearchBctMarkersRequest: typing.Type[DefaultSearchBctMarkersRequest]
    DefaultSearchLktimTreeSettingsRequest: typing.Type[DefaultSearchLktimTreeSettingsRequest]
    GroupBctMarkersByTimingUserRequest: typing.Type[GroupBctMarkersByTimingUserRequest]
    GroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest: typing.Type[GroupHistoricalLhcCollimatorAlignmentsByCollimatorNameRequest]
    GroupNamesByDeviceNameRequest: typing.Type[GroupNamesByDeviceNameRequest]
    GroupResidentStandAloneBeamProcessesByUserGroupRequest: typing.Type[GroupResidentStandAloneBeamProcessesByUserGroupRequest]
    LhcFillDirectoryCreationRequest: typing.Type[LhcFillDirectoryCreationRequest]
    LhcOfbReadingSetsRequest: typing.Type[LhcOfbReadingSetsRequest]
    LhcSetHandshakePropertyRequest: typing.Type[LhcSetHandshakePropertyRequest]
    LktimTreesRequest: typing.Type[LktimTreesRequest]
    MapLhcBlmInfoByExpertNameRequest: typing.Type[MapLhcBlmInfoByExpertNameRequest]
    MapLhcCollimatorAlignmentByCollimatorNameRequest: typing.Type[MapLhcCollimatorAlignmentByCollimatorNameRequest]
    MapLhcCollimatorInfoByNameRequest: typing.Type[MapLhcCollimatorInfoByNameRequest]
    MapResidentStandAloneBeamProcessByUserGroupRequest: typing.Type[MapResidentStandAloneBeamProcessByUserGroupRequest]
    MapStatusByTreeNameRequest: typing.Type[MapStatusByTreeNameRequest]
    SaveLktimTreeSettingsRequest: typing.Type[SaveLktimTreeSettingsRequest]
    SearchBctMarkersRequest: typing.Type[SearchBctMarkersRequest]
    SearchLktimTreeSettingsRequest: typing.Type[SearchLktimTreeSettingsRequest]
