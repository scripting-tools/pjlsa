
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.lsa.client.common.spi.japc
import cern.lsa.client.common.spi.remoting
import typing



class EnvironmentResolver:
    @staticmethod
    def isDev() -> bool: ...
    @staticmethod
    def isNext() -> bool: ...
    @staticmethod
    def isPro() -> bool: ...
    @staticmethod
    def isTest() -> bool: ...
    @staticmethod
    def isTestbed() -> bool: ...
    @staticmethod
    def isTwoTier() -> bool: ...

class ServerEnvironmentResolver:
    @staticmethod
    def isDev() -> bool: ...
    @staticmethod
    def isNext() -> bool: ...
    @staticmethod
    def isPro() -> bool: ...
    @staticmethod
    def isTest() -> bool: ...
    @staticmethod
    def isTestbed() -> bool: ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.common.spi")``.

    EnvironmentResolver: typing.Type[EnvironmentResolver]
    ServerEnvironmentResolver: typing.Type[ServerEnvironmentResolver]
    japc: cern.lsa.client.common.spi.japc.__module_protocol__
    remoting: cern.lsa.client.common.spi.remoting.__module_protocol__
