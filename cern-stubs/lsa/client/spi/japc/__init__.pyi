
import sys
if sys.version_info >= (3, 8):
    from typing import Protocol
else:
    from typing_extensions import Protocol

import cern.japc.core.spi.factory
import java.util
import typing



class AbstractNonProEnvironmentServiceConfigLookup(cern.japc.core.spi.factory.ServiceConfigLookup):
    def getConfig(self, string: str) -> typing.MutableSequence[java.util.Properties]: ...

class LsaNonproEnvironmentServiceConfigLookup(cern.japc.core.spi.factory.ServiceConfigLookup):
    def __init__(self): ...
    def getConfig(self, string: str) -> typing.MutableSequence[java.util.Properties]: ...
    @staticmethod
    def setServerPropertyFileName(string: str) -> None: ...

class ClientNonProEnvironmentServiceConfigLookup(AbstractNonProEnvironmentServiceConfigLookup):
    def __init__(self): ...

class ServerNonProEnvironmentServiceConfigLookup(AbstractNonProEnvironmentServiceConfigLookup):
    def __init__(self): ...


class __module_protocol__(Protocol):
    # A module protocol which reflects the result of ``jp.JPackage("cern.lsa.client.spi.japc")``.

    AbstractNonProEnvironmentServiceConfigLookup: typing.Type[AbstractNonProEnvironmentServiceConfigLookup]
    ClientNonProEnvironmentServiceConfigLookup: typing.Type[ClientNonProEnvironmentServiceConfigLookup]
    LsaNonproEnvironmentServiceConfigLookup: typing.Type[LsaNonproEnvironmentServiceConfigLookup]
    ServerNonProEnvironmentServiceConfigLookup: typing.Type[ServerNonProEnvironmentServiceConfigLookup]
